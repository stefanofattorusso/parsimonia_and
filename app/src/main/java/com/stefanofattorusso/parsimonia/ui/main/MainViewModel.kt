package com.stefanofattorusso.parsimonia.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.stefanofattorusso.parsimonia.domain.model.MonthId
import com.stefanofattorusso.parsimonia.domain.model.next
import com.stefanofattorusso.parsimonia.domain.model.previous
import com.stefanofattorusso.parsimonia.domain.usecase.GetTransactionsUseCase
import com.stefanofattorusso.parsimonia.domain.usecase.ObserveCurrentMonthUseCase
import com.stefanofattorusso.parsimonia.ui.main.mapper.MonthModelMapper
import com.stefanofattorusso.parsimonia.ui.main.mapper.TransactionModelMapper
import com.stefanofattorusso.parsimonia.ui.main.model.MonthModel
import com.stefanofattorusso.parsimonia.ui.main.model.TransactionModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val transactionModelMapper: TransactionModelMapper,
    private val monthModelMapper: MonthModelMapper,
    private val observeCurrentMonthUseCase: ObserveCurrentMonthUseCase,
    private val getTransactionsUseCase: GetTransactionsUseCase,
) : ViewModel() {

    companion object {
        private const val TAG = "MainViewModel"
    }

    private val _transactions = MutableLiveData<Flow<PagingData<TransactionModel>>>()
    val transactions: LiveData<Flow<PagingData<TransactionModel>>> = _transactions

    private val _viewState = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState> = _viewState

    private val _viewInteraction = MutableLiveData<ViewInteraction>()
    val viewInteraction: LiveData<ViewInteraction> = _viewInteraction

    private var monthId = MonthId()

    init {
        retrieveMonthData()
    }

    fun retrieveMonthData() {
        retrieveMonthData(monthId)
    }

    private fun retrieveMonthData(monthId: MonthId) {
        viewModelScope.launch {
            val result = observeCurrentMonthUseCase(ObserveCurrentMonthUseCase.Params(monthId))
            result
                .catch { exception -> Log.e(TAG, exception.message.toString()) }
                .map { monthModelMapper.mapFrom(it) }
                .collect {
                    retrieveTransactions(monthId)
                    _viewState.value = ViewState.Success(it)
                }
        }
    }

    fun onAddTransaction() {
        _viewInteraction.value = ViewInteraction.OnAddTransaction(monthId.toString())
    }

    private fun retrieveTransactions(monthId: MonthId) {
        val data = Pager(
            PagingConfig(
                pageSize = 20,
                enablePlaceholders = false,
                initialLoadSize = 20
            ),
        ) {
            TransactionPagingSource(monthId, getTransactionsUseCase)
        }.flow
            .cachedIn(viewModelScope)
            .map { it.map { transaction -> transactionModelMapper.mapFrom(transaction) } }
        _transactions.postValue(data)
    }

    fun onReportClicked() {
        _viewInteraction.value = ViewInteraction.OnReport(monthId.toString())
    }

    fun onPrevMonthClicked() {
        retrieveMonthData(monthId.previous())
    }

    fun onNextMonthClicked() {
        retrieveMonthData(monthId.next())
    }

    sealed class ViewState {
        data class Success(val data: MonthModel) : ViewState()
        data class Failure(val reason: String) : ViewState()
    }

    sealed class ViewInteraction {
        data class OnReport(val monthId: String) : ViewInteraction()
        data class OnAddTransaction(val monthId: String) : ViewInteraction()
    }
}
