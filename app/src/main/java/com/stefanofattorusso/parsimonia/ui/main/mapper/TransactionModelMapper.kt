package com.stefanofattorusso.parsimonia.ui.main.mapper

import com.stefanofattorusso.common.extensions.format
import com.stefanofattorusso.parsimonia.utils.Mapper
import com.stefanofattorusso.parsimonia.domain.model.Transaction
import com.stefanofattorusso.parsimonia.ui.main.model.TransactionModel
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class TransactionModelMapper @Inject constructor() : Mapper<Transaction, TransactionModel> {

    override fun mapFrom(unmapped: Transaction): TransactionModel {
        return TransactionModel(
            id = unmapped.id,
            name = unmapped.name,
            type = unmapped.type,
            amount = unmapped.amount.format(unmapped.currency),
            timestamp = parseTimestamp(unmapped.timestamp),
            category = unmapped.category
        )
    }

    override fun mapTo(unmapped: TransactionModel): Transaction {
        TODO("Not yet implemented")
    }

    private fun parseTimestamp(timestamp: Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = timestamp
        return SimpleDateFormat("EEE dd/MM", Locale.getDefault()).format(calendar.time)
    }
}
