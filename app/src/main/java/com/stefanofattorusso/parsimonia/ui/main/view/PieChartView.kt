package com.stefanofattorusso.parsimonia.ui.main.view

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.stefanofattorusso.parsimonia.R
import com.stefanofattorusso.parsimonia.databinding.PieChartViewBinding

class PieChartView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : FrameLayout(context, attrs, defStyleAttr) {

    private val binding = PieChartViewBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        binding.setUp()
    }

    private fun PieChartViewBinding.setUp() {
        with(pieChart) {
            setUsePercentValues(true)
            setExtraOffsets(EXTRA_OFFSET, EXTRA_OFFSET, EXTRA_OFFSET, EXTRA_OFFSET)
            dragDecelerationFrictionCoef = DRAG_DEC_FRICT_COEF

            isDrawHoleEnabled = true
            setHoleColor(Color.TRANSPARENT)
            holeRadius = HOLE_RADIUS
            transparentCircleRadius = TRANSPARENT_CIRCLE_RADIUS

            rotationAngle = 0f
            isRotationEnabled = false

            animateY(ANIMATION_DURATION, Easing.EaseInOutQuad)

            legend.isEnabled = false

            description = null
            setDrawEntryLabels(false)
        }
    }

    fun setData(data: PieData) {
        if (data.dataSetCount <= 0) return
        with(data.dataSet as PieDataSet) {
            colors = listOf(
                ContextCompat.getColor(context, R.color.cat_basic),
                ContextCompat.getColor(context, R.color.cat_optional),
                ContextCompat.getColor(context, R.color.cat_culture),
                ContextCompat.getColor(context, R.color.cat_extras),
                ContextCompat.getColor(context, R.color.services)
            )
            setDrawValues(false)
        }
        binding.pieChart.visibility = VISIBLE
        binding.pieChart.data = data
        binding.pieChart.invalidate()
    }

    companion object {
        private const val EXTRA_OFFSET = 5f
        private const val DRAG_DEC_FRICT_COEF = 0.95f
        private const val HOLE_RADIUS = 70f
        private const val TRANSPARENT_CIRCLE_RADIUS = 61f
        private const val ANIMATION_DURATION = 1400
    }
}
