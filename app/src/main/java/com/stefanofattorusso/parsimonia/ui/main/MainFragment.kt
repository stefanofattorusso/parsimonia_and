package com.stefanofattorusso.parsimonia.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.cardview.widget.CardView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.data.PieData
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.stefanofattorusso.parsimonia.databinding.FragmentMainBinding
import com.stefanofattorusso.parsimonia.ui.extensions.observe
import com.stefanofattorusso.parsimonia.ui.main.adapter.TransactionAdapter
import com.stefanofattorusso.parsimonia.ui.main.model.MonthModel
import com.stefanofattorusso.parsimonia.ui.report.ReportActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainFragment : com.stefanofattorusso.parsimonia.ui.base.BaseFragment<FragmentMainBinding>() {

    private val viewModel: MainViewModel by viewModels()

    private lateinit var sheetBehavior: BottomSheetBehavior<CardView>

    private val transactionsAdapter: TransactionAdapter by lazy { TransactionAdapter() }

    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            viewModel.retrieveMonthData()
        }
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): FragmentMainBinding {
        return FragmentMainBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.setUp()
        observeData()
    }

    private fun FragmentMainBinding.setUp() {
        buildTransactions()

        addFab.setOnClickListener { viewModel.onAddTransaction() }

        mainContent.settingsButton.setOnClickListener {
            resultLauncher.launch(Intent(requireContext(), com.stefanofattorusso.parsimonia.settings.SettingsActivity::class.java))
        }

        mainContent.leftButton.setOnClickListener { viewModel.onPrevMonthClicked() }
        mainContent.rightButton.setOnClickListener { viewModel.onNextMonthClicked() }
        mainContent.reportContainer.setOnClickListener { viewModel.onReportClicked() }
    }

    private fun FragmentMainBinding.buildTransactions() {
        sheetBehavior = BottomSheetBehavior.from(transactionContainer)
        sheetBehavior.isHideable = true
        transactionList.layoutManager = LinearLayoutManager(requireContext())
        transactionList.adapter = transactionsAdapter.apply {
            addLoadStateListener { loadState ->
                if (loadState.append.endOfPaginationReached) {
                    if (transactionsAdapter.itemCount < 1) {
                        sheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                    } else {
                        sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                    }
                }
            }
        }
        transactionTitle.setOnClickListener {
            if (sheetBehavior.state == BottomSheetBehavior.STATE_COLLAPSED) {
                sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            } else if (sheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }
        sheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_SETTLING -> addFab.hide()
                    BottomSheetBehavior.STATE_COLLAPSED -> addFab.show()
                    BottomSheetBehavior.STATE_HIDDEN -> addFab.show()
                    else -> Unit
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
    }

    private fun observeData() {
        observe(viewModel.viewState) { result ->
            when (result) {
                is MainViewModel.ViewState.Success -> binding.drawMonthData(result.data)
                else -> {
                    //TODO handle other states
                }
            }
        }

        observe(viewModel.viewInteraction) { interaction ->
            when (interaction) {
                is MainViewModel.ViewInteraction.OnReport -> navigateToReport(interaction.monthId)
                is MainViewModel.ViewInteraction.OnAddTransaction -> navigateToAddTransaction(interaction.monthId)
            }
        }

        observe(viewModel.transactions) { data ->
            lifecycleScope.launch {
                data.collectLatest {
                    transactionsAdapter.submitData(it)
                }
            }
        }
    }

    private fun FragmentMainBinding.drawMonthData(data: MonthModel) {
        data.run {
            mainContent.balanceTextView.text = balance
            mainContent.incomeTextView.text = income
            mainContent.outcomeTextView.text = outcome
            mainContent.monthTextView.text = name
            mainContent.fixedExpenses.text = fixedExpenses
            mainContent.servicesExpenses.text = services
            drawPieData(pieData)
        }
    }

    private fun drawPieData(data: PieData) {
        binding.mainContent.pieChart.setData(data)
    }

    private fun navigateToAddTransaction(monthId: String) {
//        startActivity(Intent(requireContext(), TransactionAddActivity::class.java)
//            .apply { putExtras(Bundle().apply { putString("MONTH_ID", monthId) }) }
//        )
    }

    private fun navigateToReport(monthId: String) {
        startActivity(Intent(requireContext(), ReportActivity::class.java)
            .apply { putExtras(Bundle().apply { putString("MONTH_ID", monthId) }) }
        )
    }
}
