package com.stefanofattorusso.parsimonia.ui.report

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import com.google.android.material.tabs.TabLayoutMediator
import com.stefanofattorusso.parsimonia.databinding.FragmentReportBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReportFragment: com.stefanofattorusso.parsimonia.ui.base.BaseFragment<FragmentReportBinding>() {

    private val viewModel: ReportViewModel by activityViewModels()

    private lateinit var reportAdapter: ReportFragmentStateAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val monthId = arguments?.getString("MONTH_ID")
        if (monthId != null) {
            viewModel.setMonthId(monthId)
        } else {
            activity?.finish()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        reportAdapter = ReportFragmentStateAdapter(this)
        binding.pager.adapter = reportAdapter
        TabLayoutMediator(binding.tabLayout, binding.pager) { tab, position ->
            tab.text = when(position) {
                0 -> "Week"
                1 -> "Month"
                2 -> "Year"
                else -> ""
            }
        }.attach()
        observeData()
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): FragmentReportBinding {
        return FragmentReportBinding.inflate(inflater, container, false)
    }

    private fun observeData() {
        viewModel.title.observe(viewLifecycleOwner) {
            (activity as AppCompatActivity).supportActionBar?.title = it
        }
    }
}
