package com.stefanofattorusso.parsimonia.ui.report

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ReportViewModel @Inject constructor(): ViewModel() {

    private var monthId: String? = null

    private val _title = MutableLiveData<String>()
    val title: LiveData<String> = _title

    fun setMonthId(id: String) {
        monthId = id
    }
}
