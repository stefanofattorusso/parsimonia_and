package com.stefanofattorusso.parsimonia.ui.report.week

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.stefanofattorusso.parsimonia.databinding.FragmentWeeklyReportBinding
import com.stefanofattorusso.parsimonia.ui.view.ConceptView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WeeklyReportFragment : com.stefanofattorusso.parsimonia.ui.base.BaseFragment<FragmentWeeklyReportBinding>() {

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): FragmentWeeklyReportBinding {
        return FragmentWeeklyReportBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.basicContainer.removeAllViews()
        binding.basicContainer.addView(ConceptView(requireContext()).apply {
            setName("supermercato")
            setAmount("18,60€")
        })

    }
}
