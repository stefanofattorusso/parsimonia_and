package com.stefanofattorusso.parsimonia.ui.report

import android.os.Bundle
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.stefanofattorusso.parsimonia.R
import com.stefanofattorusso.parsimonia.databinding.ActivityReportBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReportActivity : com.stefanofattorusso.parsimonia.ui.base.BaseActivity<ActivityReportBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                add<ReportFragment>(
                    R.id.container,
                    null,
                    intent.extras
                )
            }
        }
    }

    override fun getViewBinding() = ActivityReportBinding.inflate(layoutInflater)

}