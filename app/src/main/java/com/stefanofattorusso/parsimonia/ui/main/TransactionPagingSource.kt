package com.stefanofattorusso.parsimonia.ui.main

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.stefanofattorusso.parsimonia.domain.model.MonthId
import com.stefanofattorusso.parsimonia.domain.model.Transaction
import com.stefanofattorusso.parsimonia.domain.usecase.GetTransactionsUseCase

class TransactionPagingSource(
    private val monthId: MonthId,
    private val getTransactionsUseCase: GetTransactionsUseCase,
) : PagingSource<Int, Transaction>() {

    override fun getRefreshKey(state: PagingState<Int, Transaction>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Transaction> {
        val page = params.key ?: 0

        return try {
            val entities = getTransactionsUseCase(
                GetTransactionsUseCase.Params(
                    monthId = monthId,
                    limit = params.loadSize,
                    offset = page * params.loadSize
                )
            )

            LoadResult.Page(
                data = entities,
                prevKey = if (page == 0) null else page - 1,
                nextKey = if (entities.isEmpty()) null else page + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}