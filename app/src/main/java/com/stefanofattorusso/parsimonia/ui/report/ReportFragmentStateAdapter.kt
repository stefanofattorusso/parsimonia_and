package com.stefanofattorusso.parsimonia.ui.report

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.stefanofattorusso.parsimonia.ui.report.week.WeeklyReportFragment

class ReportFragmentStateAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount() = 3

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> WeeklyReportFragment()
            1 -> WeeklyReportFragment()
            2 -> WeeklyReportFragment()
            else -> WeeklyReportFragment()
        }
    }
}