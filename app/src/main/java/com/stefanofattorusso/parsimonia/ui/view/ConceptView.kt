package com.stefanofattorusso.parsimonia.ui.view

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.widget.TextView
import com.stefanofattorusso.parsimonia.R
import com.stefanofattorusso.parsimonia.ui.base.BaseCustomView

class ConceptView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : BaseCustomView(context, attrs, defStyleAttr, defStyleRes) {

    override val mStyleable: IntArray
        get() = IntArray(0)

    override val mLayoutId: Int
        get() = R.layout.view_concept

    override fun onLoadStyledAttributes(attrs: TypedArray) {

    }

    override fun onViewCreated() {

    }

    fun setName(name: String) {
        findViewById<TextView>(R.id.concept_text).text = name
    }

    fun setAmount(amount: String) {
        findViewById<TextView>(R.id.concept_amount).text = amount
    }
}
