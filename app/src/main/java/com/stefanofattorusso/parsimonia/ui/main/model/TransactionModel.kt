package com.stefanofattorusso.parsimonia.ui.main.model

import com.stefanofattorusso.parsimonia.domain.model.TYPE

data class TransactionModel(
    var id: Int?,
    var name: String,
    var type: TYPE,
    var amount: String,
    var timestamp: String,
    var category: com.stefanofattorusso.parsimonia.domain.model.Category?
)
