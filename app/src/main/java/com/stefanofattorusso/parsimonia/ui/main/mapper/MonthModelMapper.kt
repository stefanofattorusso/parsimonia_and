package com.stefanofattorusso.parsimonia.ui.main.mapper

import android.graphics.Color
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.stefanofattorusso.common.extensions.format
import com.stefanofattorusso.parsimonia.utils.Mapper
import com.stefanofattorusso.parsimonia.domain.model.Month
import com.stefanofattorusso.parsimonia.domain.model.TYPE
import com.stefanofattorusso.parsimonia.domain.model.Transaction
import com.stefanofattorusso.parsimonia.domain.model.name
import com.stefanofattorusso.parsimonia.ui.main.model.MonthModel
import java.math.BigDecimal
import java.math.RoundingMode
import javax.inject.Inject

class MonthModelMapper @Inject constructor() : Mapper<Month, MonthModel> {

    override fun mapFrom(unmapped: Month): MonthModel {
        return MonthModel(
            id = unmapped.id.toString(),
            name = unmapped.name(),
            income = unmapped.income.format(unmapped.currency),
            outcome = unmapped.outcome.format(unmapped.currency),
            balance = (unmapped.income.minus(unmapped.outcome)).format(unmapped.currency),
            fixedExpenses = unmapped.fixedExpenses.format(unmapped.currency),
            services = unmapped.services.format(unmapped.currency),
            pieData = mapToPieData(unmapped.expenses)
        )
    }

    override fun mapTo(unmapped: MonthModel): Month {
        TODO("Not yet implemented")
    }

    private fun mapToPieData(expenseList: List<Transaction>): PieData {
        val entries = mutableListOf<PieEntry>()

        if (expenseList.isNotEmpty()) {
            val basicAmount = getExpensesForCategory(expenseList, com.stefanofattorusso.parsimonia.domain.model.CategoryType.BASIC_NECESSITIES)
            val optionalAmount = getExpensesForCategory(expenseList, com.stefanofattorusso.parsimonia.domain.model.CategoryType.OPTIONAL)
            val cultureAmount = getExpensesForCategory(expenseList, com.stefanofattorusso.parsimonia.domain.model.CategoryType.CULTURE_FREE_TIME)
            val extrasAmount = getExpensesForCategory(expenseList, com.stefanofattorusso.parsimonia.domain.model.CategoryType.EXTRAS)

            var serviceAmount = BigDecimal.ZERO
            expenseList.filter { it.type == TYPE.SERVICE }.forEach {
                serviceAmount = serviceAmount.plus(it.amount)
            }

            val total = basicAmount.plus(optionalAmount).plus(cultureAmount).plus(extrasAmount).plus(serviceAmount)

            entries.add(PieEntry(calculatePercentage(basicAmount, total), LABEL_BASIC))
            entries.add(PieEntry(calculatePercentage(optionalAmount, total), LABEL_OPTIONAL))
            entries.add(PieEntry(calculatePercentage(cultureAmount, total), LABEL_CULTURE))
            entries.add(PieEntry(calculatePercentage(extrasAmount, total), LABEL_EXTRAS))
            entries.add(PieEntry(calculatePercentage(serviceAmount, total), LABEL_SERVICES))
        }

        val dataSet = PieDataSet(entries, "").apply {
            sliceSpace = SLICE_SPACE
        }

        return PieData(dataSet).apply {
            setValueFormatter(PercentFormatter())
            setValueTextSize(PIE_TEXT_SIZE)
            setValueTextColor(Color.WHITE)
        }
    }

    private fun getExpensesForCategory(expenses: List<Transaction>, category: com.stefanofattorusso.parsimonia.domain.model.CategoryType): BigDecimal {
        var expenseAmount = BigDecimal.ZERO
        expenses.filter { it.category?.type == category }.forEach {
            expenseAmount = expenseAmount.plus(it.amount)
        }
        return expenseAmount
    }

    private fun calculatePercentage(amount: BigDecimal, total: BigDecimal): Float {
        return amount.takeIf { it != BigDecimal.ZERO }?.multiply(ONE_HUNDRED)
            ?.divide(total, DIVIDE_SCALE, RoundingMode.HALF_EVEN)?.toFloat() ?: 0f
    }

    companion object {
        private val ONE_HUNDRED = BigDecimal("100")
        private const val DIVIDE_SCALE = 2
        private const val SLICE_SPACE = 3f
        private const val PIE_TEXT_SIZE = 12f
        private const val LABEL_BASIC = "Basic necessities"
        private const val LABEL_OPTIONAL = "Optional"
        private const val LABEL_CULTURE = "Culture and free time"
        private const val LABEL_EXTRAS = "Extras"
        private const val LABEL_SERVICES = "Services"
    }
}
