package com.stefanofattorusso.parsimonia.ui.main.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.stefanofattorusso.parsimonia.R
import com.stefanofattorusso.parsimonia.domain.model.TYPE
import com.stefanofattorusso.parsimonia.ui.main.model.TransactionModel
import com.stefanofattorusso.parsimonia.feature.transaction.view.CategoryChip

class TransactionAdapter : PagingDataAdapter<TransactionModel, TransactionViewHolder>(TransactionItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        return TransactionViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.transaction_item_layout, parent, false)
        )
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }
}

class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val nameTextView: TextView = itemView.findViewById(R.id.name)
    private val amountTextView: TextView = itemView.findViewById(R.id.amount)
    private val categoryContainer: FrameLayout = itemView.findViewById(R.id.category_container)
    private val dateTextView: TextView = itemView.findViewById(R.id.date)

    private val incomeIconDrawable: Drawable? by lazy {
        AppCompatResources.getDrawable(
            itemView.context,
            R.drawable.ic_baseline_keyboard_arrow_right_24
        )
    }

    private val outcomeIconDrawable: Drawable? by lazy {
        AppCompatResources.getDrawable(
            itemView.context,
            R.drawable.ic_baseline_keyboard_arrow_left_24
        )
    }


    fun bindTo(transaction: TransactionModel?) {
        transaction?.let { data ->
            nameTextView.text = data.name
            amountTextView.text = data.amount
            dateTextView.text = data.timestamp
            when (data.type) {
                TYPE.INCOME -> amountTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    incomeIconDrawable,
                    null,
                    null,
                    null
                )
                TYPE.OUTCOME -> amountTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    outcomeIconDrawable,
                    null,
                    null,
                    null
                )
                else -> amountTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null,
                    null,
                    null,
                    null
                )
            }
            categoryContainer.removeAllViews()
            data.category?.let { category ->
                categoryContainer.addView(
                    CategoryChip.buildChip(itemView.context, category, true).apply {
                        isCheckable = false
                        isClickable = false
                    }
                )
            }
        }
    }
}

class TransactionItemDiffCallback : DiffUtil.ItemCallback<TransactionModel>() {
    override fun areItemsTheSame(oldItem: TransactionModel, newItem: TransactionModel): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: TransactionModel, newItem: TransactionModel): Boolean =
        oldItem == newItem
}
