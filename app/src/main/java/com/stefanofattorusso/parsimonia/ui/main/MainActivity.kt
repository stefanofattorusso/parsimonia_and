package com.stefanofattorusso.parsimonia.ui.main

import com.stefanofattorusso.parsimonia.databinding.ActivityMainBinding
import com.stefanofattorusso.parsimonia.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {
    override fun getViewBinding() = ActivityMainBinding.inflate(layoutInflater)
}
