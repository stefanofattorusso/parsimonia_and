package com.stefanofattorusso.parsimonia.ui.main.model

import com.github.mikephil.charting.data.PieData

data class MonthModel(
    val id: String,
    val name: String,
    val income: String,
    val outcome: String,
    val balance: String,
    val fixedExpenses: String,
    val services: String,
    val pieData: PieData,
)
