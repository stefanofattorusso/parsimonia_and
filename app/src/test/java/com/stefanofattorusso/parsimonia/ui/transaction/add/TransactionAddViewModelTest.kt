package com.stefanofattorusso.parsimonia.ui.transaction.add

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.stefanofattorusso.parsimonia.domain.model.TYPE
import com.stefanofattorusso.parsimonia.domain.usecase.AddTransactionUseCase
import com.stefanofattorusso.parsimonia.domain.usecase.GetExpenseCategoriesUseCase
import com.stefanofattorusso.parsimonia.domain.usecase.GetNameSuggestionsUseCase
import com.stefanofattorusso.parsimonia.feature.transaction.TransactionAddViewModel
import com.stefanofattorusso.parsimonia.ui.MainDispatcherRule
import com.stefanofattorusso.parsimonia.feature.transaction.mapper.TransactionAddModelMapper
import com.stefanofattorusso.parsimonia.feature.transaction.view.TransactionAddModel
import com.stefanofattorusso.parsimonia.utils.ResultData
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@OptIn(ExperimentalCoroutinesApi::class)
class TransactionAddViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val addTransactionUseCase: AddTransactionUseCase = mockk(relaxed = true)
    private val getExpenseCategoriesUseCase: GetExpenseCategoriesUseCase = mockk(relaxed = true)
    private val transactionAddModelMapper: TransactionAddModelMapper = mockk(relaxed = true)
    private val getNameSuggestionsUseCase: GetNameSuggestionsUseCase = mockk(relaxed = true)

    private val viewModel: TransactionAddViewModel by lazy {
        TransactionAddViewModel(
            addTransactionUseCase, getExpenseCategoriesUseCase, transactionAddModelMapper, getNameSuggestionsUseCase
        )
    }

    private  val category = com.stefanofattorusso.parsimonia.domain.model.Category.new()

    @Test
    fun `When the viewmodel is initialized, Then retrieve all the categories`() = runTest {
        coEvery { getExpenseCategoriesUseCase.invoke(any()) } returns ResultData.Success(listOf(category))

        viewModel.setMonthId("id")

        assertEquals(listOf(category), viewModel.categoryList.value)
    }

    @Test
    fun `Given a new Transaction, When add is requested, Then call the use case to save the new transaction`() =
        runTest {
            coEvery { getExpenseCategoriesUseCase.invoke(any()) } returns ResultData.Success(listOf(category))

            viewModel.setMonthId("id")

            val input = TransactionAddModel(TYPE.INCOME, "name", "1", null, null)

            val success = TransactionAddViewModel.ViewState.Success

            coEvery { addTransactionUseCase.invoke(any()) } returns ResultData.Success(Unit)

            viewModel.addTransaction(input)

            coVerify { addTransactionUseCase(any()) }

            assertEquals(success, viewModel.viewState.value)
        }
}