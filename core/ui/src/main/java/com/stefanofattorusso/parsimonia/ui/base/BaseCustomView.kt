package com.stefanofattorusso.parsimonia.ui.base

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout

abstract class BaseCustomView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyleAttr, defStyleRes) {

    protected abstract val mStyleable: IntArray
    protected abstract val mLayoutId: Int

    init {
        init(context, attrs, defStyleAttr, defStyleRes)
    }

    private fun init(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0) {
        if (!isInEditMode) {
            attrs?.also {
                context.theme.obtainStyledAttributes(it, mStyleable, defStyleAttr, defStyleRes).apply {
                    try {
                        onLoadStyledAttributes(this)
                    } finally {
                        recycle()
                    }
                }
            }
            View.inflate(context, mLayoutId, this)
            onViewCreated()
        }
    }

    abstract fun onLoadStyledAttributes(attrs: TypedArray)

    abstract fun onViewCreated()

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (isInEditMode)
            View.inflate(context, mLayoutId, this)
    }
}
