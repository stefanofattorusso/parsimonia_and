package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.repository.CurrencyRepository
import com.stefanofattorusso.parsimonia.utils.ResultData
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*

@OptIn(ExperimentalCoroutinesApi::class)
class GetCurrencyListUseCaseTest {

    private val currencyRepository: CurrencyRepository = mockk(relaxed = true)

    private val useCase: GetCurrencyListUseCase by lazy { GetCurrencyListUseCase(currencyRepository) }

    @Test
    fun `When invoke is called, Then return with success`() = runBlockingTest {
        val currency = Currency.getInstance("EUR")
        val currencyList = listOf(currency)

        coEvery { currencyRepository.retrieveAllCurrencies() } returns currencyList

        val result = useCase.invoke(GetCurrencyListUseCase.Params())

        coVerify { currencyRepository.retrieveAllCurrencies() }

        assert(result is ResultData.Success)
        assertEquals(currencyList, (result as ResultData.Success).value)
    }

    @Test
    fun `When invoke is called, Then return with error`() = runBlockingTest {
        coEvery { currencyRepository.retrieveAllCurrencies() } throws Exception("Error")

        val result = useCase.invoke(GetCurrencyListUseCase.Params())

        coVerify { currencyRepository.retrieveAllCurrencies() }

        assert(result is ResultData.Error)
    }
}