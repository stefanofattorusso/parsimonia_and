package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.repository.CurrencyRepository
import com.stefanofattorusso.parsimonia.utils.ResultData
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class GetCurrentCurrencyUseCaseTest {

    private val currencyRepository: CurrencyRepository = mockk(relaxed = true)

    private val useCase: GetCurrentCurrencyUseCase by lazy { GetCurrentCurrencyUseCase(currencyRepository) }

    @Test
    fun `When invoke is called, Then return with success`() = runBlockingTest {
        val currency = "EUR"

        coEvery { currencyRepository.retrieveDefaultCurrency() } returns currency

        val result = useCase.invoke(GetCurrentCurrencyUseCase.Params())

        coVerify { currencyRepository.retrieveDefaultCurrency() }

        assert(result is ResultData.Success)
        assertEquals(currency, (result as ResultData.Success).value)
    }

    @Test
    fun `When invoke is called, Then return with error`() = runBlockingTest {
        coEvery { currencyRepository.retrieveDefaultCurrency() } throws Exception("Error")

        val result = useCase.invoke(GetCurrentCurrencyUseCase.Params())

        coVerify { currencyRepository.retrieveDefaultCurrency() }

        assert(result is ResultData.Error)
    }
}