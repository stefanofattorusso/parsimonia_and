package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.model.Month
import com.stefanofattorusso.parsimonia.domain.model.MonthId
import com.stefanofattorusso.parsimonia.domain.model.Transaction
import com.stefanofattorusso.parsimonia.domain.repository.MonthRepository
import com.stefanofattorusso.parsimonia.domain.repository.TransactionRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal

@OptIn(ExperimentalCoroutinesApi::class)
class ObserveCurrentMonthUseCaseTest {
    private val monthRepository: MonthRepository = mockk(relaxed = true)
    private val transactionRepository: TransactionRepository = mockk(relaxed = true)

    private val useCase: ObserveCurrentMonthUseCase by lazy {
        ObserveCurrentMonthUseCase(
            monthRepository,
            transactionRepository
        )
    }

    @Test
    fun `When invoke is called, Then return with success`() = runBlockingTest {
        val transaction = Transaction.new()
        val month = Month(
            MonthId(),
            BigDecimal.ZERO,
            BigDecimal.ZERO,
            "EUR",
            BigDecimal.ZERO,
            BigDecimal.ZERO,
            listOf(transaction)
        )
        val flow = flowOf(month)

        coEvery { monthRepository.observeAndCreateIfNotExists(any()) } returns flow
        coEvery { transactionRepository.getExpensesByMonth(any()) } returns listOf(transaction)

        useCase.invoke(ObserveCurrentMonthUseCase.Params(MonthId())).collect {
            assertEquals(month, it)
        }

        coVerify { monthRepository.observeAndCreateIfNotExists(any()) }
        coVerify { transactionRepository.getExpensesByMonth(any()) }

    }
}