package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.model.TYPE
import com.stefanofattorusso.parsimonia.domain.repository.TransactionRepository
import com.stefanofattorusso.parsimonia.utils.ResultData
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class GetNameSuggestionsUseCaseTest {

    private val transactionRepository: TransactionRepository = mockk(relaxed = true)

    private val useCase: GetNameSuggestionsUseCase by lazy { GetNameSuggestionsUseCase(transactionRepository) }

    @Test
    fun `When invoke is called, Then return with success`() = runBlockingTest {
        val list = listOf("name")
        val type = TYPE.INCOME

        coEvery { transactionRepository.getSuggestedNameList(any()) } returns list

        val result = useCase.invoke(GetNameSuggestionsUseCase.Params(type))

        coVerify { transactionRepository.getSuggestedNameList(any()) }

        assert(result is ResultData.Success)
        assertEquals(list, (result as ResultData.Success).value)
    }

    @Test
    fun `When invoke is called, Then return with error`() = runBlockingTest {
        val type = TYPE.INCOME

        coEvery { transactionRepository.getSuggestedNameList(any()) } throws Exception("Error")

        val result = useCase.invoke(GetNameSuggestionsUseCase.Params(type))

        coVerify { transactionRepository.getSuggestedNameList(any()) }

        assert(result is ResultData.Error)
    }
}