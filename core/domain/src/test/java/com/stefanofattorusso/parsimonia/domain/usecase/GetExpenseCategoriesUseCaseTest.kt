package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.repository.CategoryRepository
import com.stefanofattorusso.parsimonia.utils.ResultData
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class GetExpenseCategoriesUseCaseTest {

    private val categoryRepository: CategoryRepository = mockk(relaxed = true)

    private val useCase: GetExpenseCategoriesUseCase by lazy { GetExpenseCategoriesUseCase(categoryRepository) }

    @Test
    fun `When invoke is called, Then return with success`() = runBlockingTest {
        val category = com.stefanofattorusso.parsimonia.domain.model.Category.new()
        val categoryList = listOf(category)

        coEvery { categoryRepository.getAllCategories() } returns categoryList

        val result = useCase.invoke(GetExpenseCategoriesUseCase.Params())

        coVerify { categoryRepository.getAllCategories() }

        assert(result is ResultData.Success)
        assertEquals(categoryList, (result as ResultData.Success).value)
    }

    @Test
    fun `When invoke is called, Then return with error`() = runBlockingTest {
        coEvery { categoryRepository.getAllCategories() } throws Exception("Error")

        val result = useCase.invoke(GetExpenseCategoriesUseCase.Params())

        coVerify { categoryRepository.getAllCategories() }

        assert(result is ResultData.Error)
    }
}