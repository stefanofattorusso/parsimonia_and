package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.model.Transaction
import com.stefanofattorusso.parsimonia.domain.repository.TransactionRepository
import com.stefanofattorusso.parsimonia.utils.ResultData
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class AddTransactionUseCaseTest {

    private val transactionRepository: TransactionRepository = mockk(relaxed = true)

    private val useCase: AddTransactionUseCase by lazy { AddTransactionUseCase(transactionRepository) }

    @Test
    fun `When invoke is called, Then return with success`() = runBlockingTest {
        val transaction = Transaction.new()

        coEvery { transactionRepository.create(any()) } returns Unit

        val result = useCase.invoke(AddTransactionUseCase.Params(transaction))

        coVerify { transactionRepository.create(any()) }

        assert(result is ResultData.Success)
    }

    @Test
    fun `When invoke is called, Then return with error`() = runBlockingTest {
        val transaction = Transaction.new()

        coEvery { transactionRepository.create(any()) } throws Exception("Error")

        val result = useCase.invoke(AddTransactionUseCase.Params(transaction))

        coVerify { transactionRepository.create(any()) }

        assert(result is ResultData.Error)
    }
}