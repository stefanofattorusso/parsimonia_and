package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.model.MonthId
import com.stefanofattorusso.parsimonia.domain.model.Transaction
import com.stefanofattorusso.parsimonia.domain.repository.TransactionRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class GetTransactionsUseCaseTest {

    private val transactionRepository: TransactionRepository = mockk(relaxed = true)

    private val useCase: GetTransactionsUseCase by lazy { GetTransactionsUseCase(transactionRepository) }

    @Test
    fun `When invoke is called, Then return with success`() = runBlockingTest {
        val list = listOf(Transaction.new())
        val params = GetTransactionsUseCase.Params(
            MonthId(),
            10,
            0
        )

        coEvery { transactionRepository.getAllByMonth(any(), any(), any()) } returns list

        val result = useCase.invoke(params)

        coVerify { transactionRepository.getAllByMonth(any(), any(), any()) }

        assertEquals(list, result)
    }
}