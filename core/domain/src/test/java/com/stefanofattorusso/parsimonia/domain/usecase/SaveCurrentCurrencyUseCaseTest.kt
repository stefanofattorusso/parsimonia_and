package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.repository.CurrencyRepository
import com.stefanofattorusso.parsimonia.utils.ResultData
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class SaveCurrentCurrencyUseCaseTest {

    private val currencyRepository: CurrencyRepository = mockk(relaxed = true)

    private val useCase: SaveCurrentCurrencyUseCase by lazy { SaveCurrentCurrencyUseCase(currencyRepository) }

    @Test
    fun `When invoke is called, Then return with success`() = runBlockingTest {
        val currency = "EUR"

        coEvery { currencyRepository.saveDefaultCurrency(any()) } returns Unit

        val result = useCase.invoke(SaveCurrentCurrencyUseCase.Params(currency))

        coVerify { currencyRepository.saveDefaultCurrency(any()) }

        assert(result is ResultData.Success)
    }

    @Test
    fun `When invoke is called, Then return with error`() = runBlockingTest {
        val currency = "EUR"

        coEvery { currencyRepository.saveDefaultCurrency(any()) } throws Exception("Error")

        val result = useCase.invoke(SaveCurrentCurrencyUseCase.Params(currency))

        coVerify { currencyRepository.saveDefaultCurrency(any()) }

        assert(result is ResultData.Error)
    }
}