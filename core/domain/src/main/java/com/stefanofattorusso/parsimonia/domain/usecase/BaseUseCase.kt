package com.stefanofattorusso.parsimonia.domain.usecase

abstract class BaseUseCase<TYPE, PARAMS> {
    abstract suspend operator fun invoke(params: PARAMS): TYPE
}
