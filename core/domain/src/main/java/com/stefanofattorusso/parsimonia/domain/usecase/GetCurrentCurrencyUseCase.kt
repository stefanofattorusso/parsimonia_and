package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.repository.CurrencyRepository
import com.stefanofattorusso.parsimonia.utils.ResultData
import javax.inject.Inject

class GetCurrentCurrencyUseCase @Inject constructor(
    private val currencyRepository: CurrencyRepository,
) : BaseUseCase<ResultData<String>, GetCurrentCurrencyUseCase.Params>() {

    override suspend fun invoke(params: Params): ResultData<String> {
        return try {
            ResultData.Success(currencyRepository.retrieveDefaultCurrency())
        } catch (e: Exception) {
            ResultData.Error(e)
        }
    }

    class Params
}
