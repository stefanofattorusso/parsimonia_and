package com.stefanofattorusso.parsimonia.domain.model

enum class CategoryType {
    BASIC_NECESSITIES,
    OPTIONAL,
    CULTURE_FREE_TIME,
    EXTRAS
}