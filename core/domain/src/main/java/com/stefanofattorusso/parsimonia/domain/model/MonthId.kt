package com.stefanofattorusso.parsimonia.domain.model

import java.text.SimpleDateFormat
import java.util.*

class MonthId {
    val calendar: Calendar = Calendar.getInstance()
    private val formatter = SimpleDateFormat("yyyy/MM", Locale.getDefault())

    constructor()

    constructor(monthId: String) {
        calendar.time = formatter.parse(monthId)
    }

    override fun toString(): String = formatter.format(calendar.time)
}

fun MonthId.next(): MonthId {
    return this.apply { calendar.add(Calendar.MONTH, 1) }
}

fun MonthId.previous(): MonthId {
    return this.apply { calendar.add(Calendar.MONTH, -1) }
}
