package com.stefanofattorusso.parsimonia.domain.repository

import com.stefanofattorusso.parsimonia.domain.model.TYPE
import com.stefanofattorusso.parsimonia.domain.model.Transaction

interface TransactionRepository {

    suspend fun getAllByMonth(monthId: String, limit: Int, offset: Int): List<Transaction>

    suspend fun getExpensesByMonth(monthId: String): List<Transaction>

    suspend fun create(transaction: Transaction)

    suspend fun getSuggestedNameList(type: TYPE): List<String>
}
