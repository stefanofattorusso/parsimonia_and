package com.stefanofattorusso.parsimonia.domain.repository

import com.stefanofattorusso.parsimonia.domain.model.Month
import com.stefanofattorusso.parsimonia.domain.model.MonthId
import kotlinx.coroutines.flow.Flow

interface MonthRepository {

    suspend fun observeAndCreateIfNotExists(id: MonthId): Flow<Month>

    suspend fun create(): Month
}
