package com.stefanofattorusso.parsimonia.domain.repository

interface CategoryRepository {

    suspend fun getCategoryList(type: com.stefanofattorusso.parsimonia.domain.model.CategoryType): List<com.stefanofattorusso.parsimonia.domain.model.Category>

    suspend fun getAllCategories(): List<com.stefanofattorusso.parsimonia.domain.model.Category>
}
