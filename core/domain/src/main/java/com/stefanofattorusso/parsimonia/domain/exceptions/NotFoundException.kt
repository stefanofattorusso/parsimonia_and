package com.stefanofattorusso.parsimonia.domain.exceptions

class NotFoundException: Throwable("Item not found")