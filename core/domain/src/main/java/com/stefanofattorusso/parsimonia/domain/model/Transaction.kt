package com.stefanofattorusso.parsimonia.domain.model

import java.math.BigDecimal

data class Transaction(
    val id: Int? = null,
    val monthId: String,
    val name: String,
    val type: TYPE,
    val amount: BigDecimal,
    val timestamp: Long,
    val category: com.stefanofattorusso.parsimonia.domain.model.Category?,
    val currency: String? = null,
    val fixed: Boolean? = false
) {
    companion object {
        fun new() = Transaction(
            id = null,
            monthId = "",
            name = "",
            type = TYPE.UNDEFINED,
            amount = BigDecimal.ZERO,
            timestamp = -1,
            category = null,
            currency = null,
            fixed = null,
        )
    }
}

enum class TYPE(val id: Int) {
    UNDEFINED(0), INCOME(1), OUTCOME(2), SERVICE(3);

    companion object {
        fun fromInt(value: Int) = values().first { it.id == value }
    }
}
