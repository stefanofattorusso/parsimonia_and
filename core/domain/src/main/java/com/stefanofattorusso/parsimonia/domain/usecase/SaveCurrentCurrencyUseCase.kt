package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.repository.CurrencyRepository
import com.stefanofattorusso.parsimonia.utils.ResultData
import javax.inject.Inject

class SaveCurrentCurrencyUseCase @Inject constructor(
    private val currencyRepository: CurrencyRepository,
) : BaseUseCase<ResultData<Unit>, SaveCurrentCurrencyUseCase.Params>() {

    override suspend fun invoke(params: Params): ResultData<Unit> {
        return try {
            ResultData.Success(currencyRepository.saveDefaultCurrency(params.currency))
        } catch (e: Exception) {
            ResultData.Error(e)
        }
    }

    data class Params(
        val currency: String
    )
}
