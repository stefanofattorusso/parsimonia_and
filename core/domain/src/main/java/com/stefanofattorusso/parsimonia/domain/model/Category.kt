package com.stefanofattorusso.parsimonia.domain.model

data class Category(
    val id: Int,
    val name: String,
    val type: com.stefanofattorusso.parsimonia.domain.model.CategoryType,
    val icon: Int?
) {
    companion object {
        fun new() = com.stefanofattorusso.parsimonia.domain.model.Category(
            0,
            "",
            com.stefanofattorusso.parsimonia.domain.model.CategoryType.BASIC_NECESSITIES,
            null
        )
    }
}
