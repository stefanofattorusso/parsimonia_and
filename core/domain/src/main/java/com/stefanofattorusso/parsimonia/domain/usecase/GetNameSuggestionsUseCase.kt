package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.model.TYPE
import com.stefanofattorusso.parsimonia.domain.repository.TransactionRepository
import com.stefanofattorusso.parsimonia.utils.ResultData
import javax.inject.Inject

class GetNameSuggestionsUseCase @Inject constructor(
    private val transactionRepository: TransactionRepository,
) : BaseUseCase<ResultData<List<String>>, GetNameSuggestionsUseCase.Params>() {

    override suspend fun invoke(params: Params): ResultData<List<String>> {
        return try {
            ResultData.Success(transactionRepository.getSuggestedNameList(params.type))
        } catch (e: Exception) {
            ResultData.Error(e)
        }
    }

    data class Params(val type: TYPE)
}
