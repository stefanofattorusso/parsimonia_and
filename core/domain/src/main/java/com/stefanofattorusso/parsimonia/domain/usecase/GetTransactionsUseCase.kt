package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.model.MonthId
import com.stefanofattorusso.parsimonia.domain.model.Transaction
import com.stefanofattorusso.parsimonia.domain.repository.TransactionRepository
import javax.inject.Inject

class GetTransactionsUseCase @Inject constructor(
    private val transactionRepository: TransactionRepository,
) : BaseUseCase<List<Transaction>, GetTransactionsUseCase.Params>() {

    override suspend fun invoke(params: Params): List<Transaction> {
        return transactionRepository.getAllByMonth(params.monthId.toString(), params.limit, params.offset)
    }

    data class Params(
        val monthId: MonthId,
        val limit: Int,
        val offset: Int,
    )
}
