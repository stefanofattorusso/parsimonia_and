package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.model.Transaction
import com.stefanofattorusso.parsimonia.domain.repository.TransactionRepository
import com.stefanofattorusso.parsimonia.utils.ResultData
import javax.inject.Inject

class AddTransactionUseCase @Inject constructor(
    private val transactionRepository: TransactionRepository,
) : BaseUseCase<ResultData<Unit>, AddTransactionUseCase.Params>() {

    override suspend fun invoke(params: Params): ResultData<Unit> {
        return try {
            ResultData.Success(transactionRepository.create(params.transaction))
        } catch (e: Exception) {
            ResultData.Error(e)
        }
    }

    data class Params(
        val transaction: Transaction
    )
}
