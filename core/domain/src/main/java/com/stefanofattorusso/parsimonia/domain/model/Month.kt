package com.stefanofattorusso.parsimonia.domain.model

import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

data class Month(
    val id: MonthId,
    val income: BigDecimal,
    val outcome: BigDecimal,
    val currency: String?,
    val fixedExpenses: BigDecimal,
    val services: BigDecimal,
    val expenses: List<Transaction>,
) {
    companion object {
        fun new(id: MonthId? = null): Month {
            return Month(
                id = id ?: MonthId(),
                income = BigDecimal.ZERO,
                outcome = BigDecimal.ZERO,
                currency = null,
                fixedExpenses = BigDecimal.ZERO,
                services = BigDecimal.ZERO,
                expenses = emptyList()
            )
        }
    }
}

fun Month.name(): String {
    return SimpleDateFormat("MMMM yyyy", Locale.getDefault()).format(id.calendar.time)
}

fun Month.updateWithIncome(amount: BigDecimal) = this.copy(income = income.plus(amount))

fun Month.updateWithOutcome(amount: BigDecimal, fixed: Boolean?): Month {
    var month = this.copy(outcome = outcome.plus(amount))
    if (fixed == true) {
        month = month.copy(fixedExpenses = fixedExpenses.plus(amount))
    }
    return month
}

fun Month.updateWithService(amount: BigDecimal): Month {
    val month = this.copy(outcome = outcome.plus(amount))
    return month.copy(services = services.plus(amount))
}
