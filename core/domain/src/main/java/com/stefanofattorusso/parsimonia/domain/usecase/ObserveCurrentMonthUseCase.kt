package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.model.Month
import com.stefanofattorusso.parsimonia.domain.model.MonthId
import com.stefanofattorusso.parsimonia.domain.repository.MonthRepository
import com.stefanofattorusso.parsimonia.domain.repository.TransactionRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ObserveCurrentMonthUseCase @Inject constructor(
    private val monthRepository: MonthRepository,
    private val transactionRepository: TransactionRepository,
) : BaseUseCase<Flow<Month>, ObserveCurrentMonthUseCase.Params>() {

    override suspend fun invoke(params: Params): Flow<Month> {
        return monthRepository.observeAndCreateIfNotExists(params.monthId).map { month ->
            val expenses = transactionRepository.getExpensesByMonth(month.id.toString())
            month.copy(expenses = expenses)
        }
    }

    data class Params(
        val monthId: MonthId
    )
}
