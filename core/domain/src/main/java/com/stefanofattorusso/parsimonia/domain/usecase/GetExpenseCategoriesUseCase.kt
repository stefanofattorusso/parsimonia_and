package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.repository.CategoryRepository
import com.stefanofattorusso.parsimonia.utils.ResultData
import javax.inject.Inject

class GetExpenseCategoriesUseCase @Inject constructor(
    private val categoryRepository: CategoryRepository,
) : BaseUseCase<ResultData<List<com.stefanofattorusso.parsimonia.domain.model.Category>>, GetExpenseCategoriesUseCase.Params>() {

    override suspend fun invoke(params: Params): ResultData<List<com.stefanofattorusso.parsimonia.domain.model.Category>> {
        return try {
            ResultData.Success(categoryRepository.getAllCategories())
        } catch (e: Exception) {
            ResultData.Error(e)
        }
    }

    class Params
}
