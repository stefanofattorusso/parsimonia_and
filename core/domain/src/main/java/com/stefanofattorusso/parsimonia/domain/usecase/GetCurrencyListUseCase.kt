package com.stefanofattorusso.parsimonia.domain.usecase

import com.stefanofattorusso.parsimonia.domain.repository.CurrencyRepository
import com.stefanofattorusso.parsimonia.utils.ResultData
import java.util.*
import javax.inject.Inject

class GetCurrencyListUseCase @Inject constructor(
    private val currencyRepository: CurrencyRepository,
) : BaseUseCase<ResultData<List<Currency>>, GetCurrencyListUseCase.Params>() {

    override suspend fun invoke(params: Params): ResultData<List<Currency>> {
        return try {
            ResultData.Success(currencyRepository.retrieveAllCurrencies())
        } catch (e: Exception) {
            ResultData.Error(e)
        }
    }

    class Params
}
