package com.stefanofattorusso.parsimonia.domain.repository

import java.util.*

interface CurrencyRepository {

    fun retrieveAllCurrencies(): List<Currency>

    fun retrieveDefaultCurrency(): String

    fun saveDefaultCurrency(currencyCode: String)
}