package com.stefanofattorusso.parsimonia.utils

/**
 * Interface for model mappers. It provides helper methods that facilitate
 * retrieving of models from outer data source layers
 *
 * @param <E> the cached model input type
 * @param <D> the model return type
 */
interface Mapper<E, D> {

    fun mapFrom(unmapped: E): D

    fun mapTo(unmapped: D): E

}
