package com.stefanofattorusso.common.extensions

import java.math.BigDecimal
import java.math.RoundingMode
import java.text.NumberFormat
import java.util.*

fun BigDecimal.format(currencyCode: String?): String {
    val displayAmount = this.setScale(2, RoundingMode.HALF_EVEN)
    val currency = Currency.getInstance(currencyCode)
    val currencyFormat = NumberFormat.getCurrencyInstance()
    currencyFormat.currency = currency
    currencyFormat.minimumFractionDigits = 0
    currencyFormat.maximumFractionDigits = currency.defaultFractionDigits
    return currencyFormat.format(displayAmount.toDouble())
}

fun Boolean?.toInt() = if (this == true) 1 else 0

fun Int?.toBoolean() = this == 1
