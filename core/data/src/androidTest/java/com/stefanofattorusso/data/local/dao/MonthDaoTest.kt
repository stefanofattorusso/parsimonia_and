package com.stefanofattorusso.data.local.dao

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.stefanofattorusso.parsimonia.data.local.dao.MonthDao
import com.stefanofattorusso.parsimonia.data.local.database.ParsimoniaDatabase
import com.stefanofattorusso.parsimonia.data.local.entity.MonthEntity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class MonthDaoTest {

    private lateinit var monthDao: MonthDao
    private lateinit var db: ParsimoniaDatabase

    private val monthEntity = MonthEntity(
        "2023/01",
        "0",
        "0",
        "0",
        "0"
    )

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context,
            ParsimoniaDatabase::class.java
        ).build()
        monthDao = db.monthDao()
    }

    @Test
    fun testObserveMonthById() = runTest {

        monthDao.insertAll(monthEntity)

        val result = monthDao.observe("2023/01").first()

        assertEquals("2023/01", result.first().id)
    }

    @Test
    fun testObserveMonthByIdNotFound() = runTest {

        monthDao.insertAll(monthEntity)

        val result = monthDao.observe("2023/02").first()

        assert(result.isEmpty())
    }

    @Test
    fun testUpdateMonth() = runTest {

        monthDao.insertAll(monthEntity)
        monthDao.update(monthEntity.copy(income = "12"))

        val result = monthDao.get("2023/01").first()

        assertEquals("12", result.income)
    }

    @Test
    fun testDeleteMonth() = runTest {

        monthDao.insertAll(monthEntity)

        val result = monthDao.get("2023/01")

        assert(result.isNotEmpty())

        monthDao.delete(monthEntity)

        val newResult = monthDao.get("2023/01")

        assert(newResult.isEmpty())
    }
}