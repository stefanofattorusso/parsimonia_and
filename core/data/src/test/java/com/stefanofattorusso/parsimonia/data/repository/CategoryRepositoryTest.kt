package com.stefanofattorusso.parsimonia.data.repository

import com.stefanofattorusso.parsimonia.data.local.entity.CategoryEntity
import com.stefanofattorusso.parsimonia.data.local.mapper.CategoryLocalMapper
import com.stefanofattorusso.parsimonia.data.local.source.DeviceDataSource
import com.stefanofattorusso.parsimonia.domain.repository.CategoryRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestCoroutineScheduler
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class CategoryRepositoryTest {

    private val dispatcher = StandardTestDispatcher(TestCoroutineScheduler())
    private val deviceDataSource: DeviceDataSource = mockk(relaxed = true)
    private val categoryLocalMapper: CategoryLocalMapper = mockk(relaxed = true)

    private val repository: CategoryRepository by lazy {
        CategoryRepositoryImpl(dispatcher, deviceDataSource, categoryLocalMapper)
    }

    private val catEntity = CategoryEntity(0, "Cat1", "BASIC_NECESSITIES", "")
    private val category = com.stefanofattorusso.parsimonia.domain.model.Category(
        0,
        "Cat1",
        com.stefanofattorusso.parsimonia.domain.model.CategoryType.BASIC_NECESSITIES,
        null
    )

    private val categoryEntities = listOf(catEntity)
    private val categories = listOf(category)

    @Before
    fun setUp() {
        every { deviceDataSource.getCategoryList() } returns categoryEntities
        every { categoryLocalMapper.mapFrom(catEntity) } returns category
    }

    @Test
    fun `Given a category type, When category list is requested, Then return the list of categories`() =
        runTest(dispatcher.scheduler) {
            val result = repository.getCategoryList(com.stefanofattorusso.parsimonia.domain.model.CategoryType.BASIC_NECESSITIES)

            verify { deviceDataSource.getCategoryList() }

            assertEquals(categories, result)
        }

    @Test
    fun `Given a category type, When category list is requested, Then return an empty list of categories`() =
        runTest(dispatcher.scheduler) {
            val result = repository.getCategoryList(com.stefanofattorusso.parsimonia.domain.model.CategoryType.EXTRAS)

            verify { deviceDataSource.getCategoryList() }

            assert(result.isEmpty())
        }

    @Test
    fun `When all the categories are requested, Then return all the categories available`() =
        runTest(dispatcher.scheduler) {
            val result = repository.getAllCategories()

            verify { deviceDataSource.getCategoryList() }

            assertEquals(categories, result)
        }
}