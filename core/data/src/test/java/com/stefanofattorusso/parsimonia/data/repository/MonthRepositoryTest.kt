package com.stefanofattorusso.parsimonia.data.repository

import com.stefanofattorusso.parsimonia.data.local.dao.MonthDao
import com.stefanofattorusso.parsimonia.data.local.entity.MonthEntity
import com.stefanofattorusso.parsimonia.data.local.mapper.MonthLocalMapper
import com.stefanofattorusso.parsimonia.data.local.source.DeviceDataSource
import com.stefanofattorusso.parsimonia.domain.model.Month
import com.stefanofattorusso.parsimonia.domain.model.MonthId
import com.stefanofattorusso.parsimonia.domain.repository.MonthRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestCoroutineScheduler
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal

@OptIn(ExperimentalCoroutinesApi::class)
class MonthRepositoryTest {

    private val dispatcher = StandardTestDispatcher(TestCoroutineScheduler())
    private val deviceDataSource: DeviceDataSource = mockk(relaxed = true)
    private val monthDao: MonthDao = mockk(relaxed = true)
    private val monthLocalMapper: MonthLocalMapper = mockk(relaxed = true)

    private val repository: MonthRepository by lazy {
        MonthRepositoryImpl(dispatcher, monthDao, monthLocalMapper, deviceDataSource)
    }

    private val monthId = MonthId("2023/01")
    private val entity = MonthEntity(
        "2023/01",
        "0",
        "0",
        "0",
        "0"
    )
    private val month = Month(
        monthId,
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        "EUR",
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        emptyList()
    )

    @Before
    fun setUp() {
        every { monthLocalMapper.mapTo(any()) } returns entity
        every { deviceDataSource.retrieveDefaultCurrency() } returns "EUR"
        every { monthLocalMapper.mapFrom(entity) } returns month
    }

    @Test
    fun `Given a month id, When the month data is requested, Then return the data if available`() =
        runTest(dispatcher.scheduler) {
            every { monthDao.observe(any()) } returns flow { emit(listOf(entity)) }

            val result = repository.observeAndCreateIfNotExists(MonthId()).first()

            verify { deviceDataSource.retrieveDefaultCurrency() }

            assertEquals(month, result)
        }

    @Test
    fun `Given a month id, When the month data is requested, Then return a new month data`() =
        runTest(dispatcher.scheduler) {
            every { monthDao.observe(any()) } returns flow { emit(emptyList()) }

            val result = repository.observeAndCreateIfNotExists(MonthId()).first()
            advanceUntilIdle()
            verify { monthLocalMapper.mapTo(any()) }
            verify { deviceDataSource.retrieveDefaultCurrency() }

            assertEquals(month, result)
        }
}