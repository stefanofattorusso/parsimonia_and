package com.stefanofattorusso.parsimonia.data.repository

import com.stefanofattorusso.parsimonia.data.local.source.DeviceDataSource
import com.stefanofattorusso.parsimonia.domain.repository.CurrencyRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.util.*

class CurrencyRepositoryTest {

    private val deviceDataSource: DeviceDataSource = mockk(relaxed = true)

    private val repository: CurrencyRepository by lazy {
        CurrencyRepositoryImpl(deviceDataSource)
    }

    private val currencies = listOf(Currency.getInstance(Locale.getDefault()))
    private val currencyCode = "EUR"

    @Before
    fun setUp() {
        every { deviceDataSource.retrieveAllCurrencies() } returns currencies
        every { deviceDataSource.retrieveDefaultCurrency() } returns currencyCode
    }

    @Test
    fun `When the currency list is requested, Then return the list of available currencies`() {
        val result = repository.retrieveAllCurrencies()

        verify { deviceDataSource.retrieveAllCurrencies() }

        assertEquals(currencies, result)
    }

    @Test
    fun `When the default currency is requested, Then return the available currency`() {
        val result = repository.retrieveDefaultCurrency()

        verify { deviceDataSource.retrieveDefaultCurrency() }

        assertEquals(currencyCode, result)
    }

    @Test
    fun `When saving the default currency is requested, Then verify that the function is called`() {
        repository.saveDefaultCurrency(currencyCode)

        verify { deviceDataSource.saveDefaultCurrency(currencyCode) }

    }
}