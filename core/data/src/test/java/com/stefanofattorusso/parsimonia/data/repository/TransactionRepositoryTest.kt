package com.stefanofattorusso.parsimonia.data.repository

import com.stefanofattorusso.parsimonia.data.local.dao.MonthDao
import com.stefanofattorusso.parsimonia.data.local.dao.TransactionDao
import com.stefanofattorusso.parsimonia.data.local.dao.TransactionNameDao
import com.stefanofattorusso.parsimonia.data.local.entity.CategoryEntity
import com.stefanofattorusso.parsimonia.data.local.entity.MonthEntity
import com.stefanofattorusso.parsimonia.data.local.entity.TransactionEntity
import com.stefanofattorusso.parsimonia.data.local.mapper.CategoryLocalMapper
import com.stefanofattorusso.parsimonia.data.local.mapper.MonthLocalMapper
import com.stefanofattorusso.parsimonia.data.local.mapper.TransactionLocalMapper
import com.stefanofattorusso.parsimonia.data.local.source.DeviceDataSource
import com.stefanofattorusso.parsimonia.domain.model.Month
import com.stefanofattorusso.parsimonia.domain.model.MonthId
import com.stefanofattorusso.parsimonia.domain.model.TYPE
import com.stefanofattorusso.parsimonia.domain.model.Transaction
import com.stefanofattorusso.parsimonia.domain.repository.TransactionRepository
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestCoroutineScheduler
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal

@OptIn(ExperimentalCoroutinesApi::class)
class TransactionRepositoryTest {

    private val dispatcher = StandardTestDispatcher(TestCoroutineScheduler())
    private val deviceDataSource: DeviceDataSource = mockk(relaxed = true)
    private val monthDao: MonthDao = mockk(relaxed = true)
    private val monthLocalMapper: MonthLocalMapper = mockk(relaxed = true)
    private val transactionDao: TransactionDao = mockk(relaxed = true)
    private val transactionLocalMapper: TransactionLocalMapper = mockk(relaxed = true)
    private val transactionNameDao: TransactionNameDao = mockk(relaxed = true)
    private val categoryLocalMapper: CategoryLocalMapper = mockk(relaxed = true)

    private val repository: TransactionRepository by lazy {
        TransactionRepositoryImpl(
            dispatcher,
            transactionDao,
            transactionLocalMapper,
            monthDao,
            monthLocalMapper,
            transactionNameDao,
            deviceDataSource,
            categoryLocalMapper
        )
    }

    private val catEntity = CategoryEntity(0, "Cat1", "BASIC_NECESSITIES", "")
    private val category = com.stefanofattorusso.parsimonia.domain.model.Category(
        0,
        "Cat1",
        com.stefanofattorusso.parsimonia.domain.model.CategoryType.BASIC_NECESSITIES,
        null
    )

    private val categoryEntities = listOf(catEntity)

    private val monthId = MonthId("2023/01")
    private val monthEntity = MonthEntity(
        "2023/01",
        "0",
        "0",
        "0",
        "0"
    )
    private val month = Month(
        monthId,
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        "EUR",
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        emptyList()
    )

    private val transactionEntity = TransactionEntity(
        0,
        monthId.toString(),
        "",
        1,
        "10",
        0,
        category.type.name,
        category.id,
        0
    )

    private val transaction = Transaction(
        0,
        monthId.toString(),
        "",
        TYPE.INCOME,
        BigDecimal.TEN,
        0,
        category,
        "EUR",
        false
    )

    private val transactionList = listOf(transaction)

    @Before
    fun setUp() {
        every { deviceDataSource.getCategoryList() } returns categoryEntities
        every { categoryLocalMapper.mapFrom(catEntity) } returns category
        every { monthLocalMapper.mapTo(any()) } returns monthEntity
        every { deviceDataSource.retrieveDefaultCurrency() } returns "EUR"
        every { monthLocalMapper.mapFrom(monthEntity) } returns month
    }

    @Test
    fun `Given a month, When all transactions are requested, Then return the list of available transactions`() =
        runTest(dispatcher.scheduler) {

            every { transactionDao.getPagedList(any(), any(), any()) } returns listOf(transactionEntity)
            every { transactionLocalMapper.mapFrom(any()) } returns transaction

            val result = repository.getAllByMonth(monthId = monthId.toString(), 1, 0)

            verify { deviceDataSource.getCategoryList() }
            verify { deviceDataSource.retrieveDefaultCurrency() }
            verify { transactionDao.getPagedList(any(), any(), any()) }

            assertEquals(transactionList, result)
        }

    @Test
    fun `Given a month, When expenses are requested, Then return the list of available transactions`() =
        runTest(dispatcher.scheduler) {

            every { transactionLocalMapper.mapFrom(any()) } returns transaction
            coEvery { transactionDao.getExpensesByMonth(any()) } returns listOf(transactionEntity)

            val result = repository.getExpensesByMonth(monthId = monthId.toString())
            advanceUntilIdle()

            verify { deviceDataSource.getCategoryList() }

            assertEquals(transactionList, result)
        }

    @Test
    fun `Given a transaction, When creation is required, Then insert the transaction into the db`() =
        runTest(dispatcher.scheduler) {
            every { transactionLocalMapper.mapTo(any()) } returns transactionEntity
            coEvery { monthDao.get(any()) } returns listOf(monthEntity)

            repository.create(transaction)
            advanceUntilIdle()

            coVerify { transactionDao.insertAll(any()) }
            coVerify { monthDao.update(any()) }
            coVerify { transactionNameDao.insert(any()) }
            coVerify { transactionNameDao.updateCounter(any()) }
        }

    @Test
    fun `When suggested names is requested, Then return a list of saved names`() =
        runTest(dispatcher.scheduler) {

            repository.getSuggestedNameList(TYPE.INCOME)

            coVerify { transactionNameDao.getMostUsedNames(any()) }
        }
}