package com.stefanofattorusso.parsimonia.data.local.mapper

import com.stefanofattorusso.parsimonia.data.local.entity.MonthEntity
import com.stefanofattorusso.parsimonia.domain.model.Month
import com.stefanofattorusso.parsimonia.domain.model.MonthId
import java.math.BigDecimal
import javax.inject.Inject

class MonthLocalMapper @Inject constructor() :
    com.stefanofattorusso.parsimonia.utils.Mapper<MonthEntity, Month> {

    override fun mapFrom(unmapped: MonthEntity): Month {
        return Month(
            id = MonthId(unmapped.id),
            income = BigDecimal(unmapped.income),
            outcome = BigDecimal(unmapped.outcome),
            currency = null,
            fixedExpenses = BigDecimal(unmapped.fixedExpenses),
            services = BigDecimal(unmapped.services),
            expenses = emptyList()
        )
    }

    override fun mapTo(unmapped: Month): MonthEntity {
        return MonthEntity(
            id = unmapped.id.toString(),
            income = unmapped.income.toPlainString(),
            outcome = unmapped.outcome.toPlainString(),
            fixedExpenses = unmapped.fixedExpenses.toPlainString(),
            services = unmapped.services.toPlainString()
        )
    }
}