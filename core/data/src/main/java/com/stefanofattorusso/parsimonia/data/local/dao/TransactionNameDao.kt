package com.stefanofattorusso.parsimonia.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.stefanofattorusso.parsimonia.data.local.entity.TransactionNameEntity

@Dao
interface TransactionNameDao {

    @Query("SELECT * FROM `transactionName` WHERE name = :name")
    suspend fun get(name: String): TransactionNameEntity?

    @Query("SELECT name FROM `transactionName` WHERE type = :type ORDER BY counter DESC LIMIT 5")
    suspend fun getMostUsedNames(type: Int): List<String>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(name: TransactionNameEntity)

    @Query("UPDATE `transactionName` SET counter = counter + 1 WHERE name = :name")
    suspend fun updateCounter(name: String)
}
