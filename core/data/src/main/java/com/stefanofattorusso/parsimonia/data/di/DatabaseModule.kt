package com.stefanofattorusso.parsimonia.data.di

import android.content.Context
import androidx.room.Room
import com.stefanofattorusso.parsimonia.data.local.dao.AccountBookDao
import com.stefanofattorusso.parsimonia.data.local.dao.MonthDao
import com.stefanofattorusso.parsimonia.data.local.dao.TransactionDao
import com.stefanofattorusso.parsimonia.data.local.dao.TransactionNameDao
import com.stefanofattorusso.parsimonia.data.local.database.ParsimoniaDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Provides
    fun provideAccountBookDao(database: ParsimoniaDatabase): AccountBookDao {
        return database.accountBookDao()
    }

    @Provides
    fun provideMonthDao(database: ParsimoniaDatabase): MonthDao {
        return database.monthDao()
    }

    @Provides
    fun provideTransactionDao(database: ParsimoniaDatabase): TransactionDao {
        return database.transactionDao()
    }

    @Provides
    fun provideTransactionNameDao(database: ParsimoniaDatabase): TransactionNameDao {
        return database.transactionNameDao()
    }

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): ParsimoniaDatabase {
        return Room.databaseBuilder(
            context,
            ParsimoniaDatabase::class.java,
            "parsimonia-database"
        ).fallbackToDestructiveMigration().build()
    }
}
