package com.stefanofattorusso.parsimonia.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "month")
data class MonthEntity(
    @PrimaryKey val id: String,
    @ColumnInfo val income: String,
    @ColumnInfo val outcome: String,
    @ColumnInfo val fixedExpenses: String,
    @ColumnInfo val services: String
)