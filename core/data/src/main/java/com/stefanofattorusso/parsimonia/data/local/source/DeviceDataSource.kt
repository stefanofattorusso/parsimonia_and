package com.stefanofattorusso.parsimonia.data.local.source

import android.content.Context
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.stefanofattorusso.parsimonia.data.R
import com.stefanofattorusso.parsimonia.data.local.entity.CategoryEntity
import dagger.hilt.android.qualifiers.ApplicationContext
import java.lang.reflect.Type
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DeviceDataSource @Inject constructor(
    @ApplicationContext private val context: Context
) {

    private var list: List<CategoryEntity> = emptyList()

    fun getCategoryList(): List<CategoryEntity> {
        return list.ifEmpty {
            val categoryListType: Type = object : TypeToken<ArrayList<CategoryEntity?>?>() {}.type
            list = context.resources.openRawResource(R.raw.categories)
                .bufferedReader()
                .use { reader -> Gson().fromJson(reader, categoryListType) }
            list
        }
    }

    fun retrieveAllCurrencies(): List<Currency> {
        val list = Currency.getAvailableCurrencies().toList().sortedBy { it.currencyCode }
        val pair = list.partition { it.symbol.length == 1 }
        val final = mutableListOf<Currency>()
        final.addAll(pair.first)
        final.addAll(pair.second)
        final.remove(Currency.getInstance("XXX"))
        return final.toList()
    }

    fun retrieveDefaultCurrency(): String {
        val defaultCurrency = Currency.getInstance(Locale.getDefault()).currencyCode
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(CURRENCY_CODE, defaultCurrency) ?: defaultCurrency
    }

    fun saveDefaultCurrency(currencyCode: String) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        sharedPreferences.edit {
            putString(CURRENCY_CODE, currencyCode)
        }
    }

    companion object {
        private const val CURRENCY_CODE = "currencyCode"
    }
}
