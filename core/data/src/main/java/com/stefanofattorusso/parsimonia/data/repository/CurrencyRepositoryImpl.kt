package com.stefanofattorusso.parsimonia.data.repository

import com.stefanofattorusso.parsimonia.data.local.source.DeviceDataSource
import com.stefanofattorusso.parsimonia.domain.repository.CurrencyRepository
import java.util.*
import javax.inject.Inject

class CurrencyRepositoryImpl @Inject constructor(
    private val deviceDataSource: DeviceDataSource
): CurrencyRepository {

    override fun retrieveAllCurrencies(): List<Currency> {
        return deviceDataSource.retrieveAllCurrencies()
    }

    override fun retrieveDefaultCurrency(): String {
        return deviceDataSource.retrieveDefaultCurrency()
    }

    override fun saveDefaultCurrency(currencyCode: String) {
        deviceDataSource.saveDefaultCurrency(currencyCode)
    }
}
