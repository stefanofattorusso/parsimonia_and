package com.stefanofattorusso.parsimonia.data.repository

import com.stefanofattorusso.parsimonia.data.di.IoDispatcher
import com.stefanofattorusso.parsimonia.data.local.mapper.CategoryLocalMapper
import com.stefanofattorusso.parsimonia.data.local.source.DeviceDataSource
import com.stefanofattorusso.parsimonia.domain.repository.CategoryRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CategoryRepositoryImpl @Inject constructor(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    private val deviceDataSource: DeviceDataSource,
    private val categoryLocalMapper: CategoryLocalMapper
) : CategoryRepository {

    override suspend fun getCategoryList(type: com.stefanofattorusso.parsimonia.domain.model.CategoryType): List<com.stefanofattorusso.parsimonia.domain.model.Category> {
        return withContext(ioDispatcher) {
            deviceDataSource.getCategoryList()
                .map {
                    categoryLocalMapper.mapFrom(it)
                }.filter {
                    it.type == type
                }
        }
    }

    override suspend fun getAllCategories(): List<com.stefanofattorusso.parsimonia.domain.model.Category> {
        return withContext(ioDispatcher) {
            deviceDataSource.getCategoryList()
                .map { categoryLocalMapper.mapFrom(it) }
        }
    }
}
