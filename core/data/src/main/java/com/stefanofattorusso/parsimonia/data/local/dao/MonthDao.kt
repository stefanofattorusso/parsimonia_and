package com.stefanofattorusso.parsimonia.data.local.dao

import androidx.room.*
import com.stefanofattorusso.parsimonia.data.local.entity.MonthEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface MonthDao {

    @Query("SELECT * FROM month WHERE id = :id")
    fun observe(id: String): Flow<List<MonthEntity>>

    @Query("SELECT * FROM month WHERE id = :id")
    suspend fun get(id: String): List<MonthEntity>

    @Query("SELECT * FROM month")
    fun getAll(): Flow<List<MonthEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(vararg monthEntities: MonthEntity)

    @Update
    suspend fun update(vararg monthEntities: MonthEntity)

    @Delete
    suspend fun delete(monthEntity: MonthEntity)
}
