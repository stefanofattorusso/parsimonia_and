package com.stefanofattorusso.parsimonia.data.local.entity

data class CategoryEntity(
    val id: Int,
    val name: String,
    val type: String,
    val icon: String
)
