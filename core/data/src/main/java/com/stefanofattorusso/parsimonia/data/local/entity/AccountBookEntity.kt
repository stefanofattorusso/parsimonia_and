package com.stefanofattorusso.parsimonia.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "account_book")
data class AccountBookEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0
)
