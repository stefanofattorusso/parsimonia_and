package com.stefanofattorusso.parsimonia.data.repository

import com.stefanofattorusso.parsimonia.data.di.IoDispatcher
import com.stefanofattorusso.parsimonia.data.local.dao.MonthDao
import com.stefanofattorusso.parsimonia.data.local.mapper.MonthLocalMapper
import com.stefanofattorusso.parsimonia.data.local.source.DeviceDataSource
import com.stefanofattorusso.parsimonia.domain.model.Month
import com.stefanofattorusso.parsimonia.domain.model.MonthId
import com.stefanofattorusso.parsimonia.domain.repository.MonthRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MonthRepositoryImpl @Inject constructor(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    private val monthDao: MonthDao,
    private val monthLocalMapper: MonthLocalMapper,
    private val deviceDataSource: DeviceDataSource
) : MonthRepository {

    override suspend fun observeAndCreateIfNotExists(id: MonthId): Flow<Month> {
        return withContext(context = ioDispatcher) {
            monthDao.observe(id.toString())
                .distinctUntilChanged()
                .map { list ->
                    if (list.isEmpty()) {
                        val entity = monthLocalMapper.mapTo(Month.new(id))
                        monthDao.insertAll(entity)
                        entity
                    } else {
                        list.first()
                    }
                }
                .map { month ->
                    val currency = deviceDataSource.retrieveDefaultCurrency()
                    monthLocalMapper.mapFrom(month).copy(currency = currency)
                }
        }
    }

    override suspend fun create(): Month {
        return withContext(context = ioDispatcher) {
            val currentMonth = Month.new()
            monthDao.insertAll(monthLocalMapper.mapTo(currentMonth))
            currentMonth
        }
    }
}
