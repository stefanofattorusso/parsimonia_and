package com.stefanofattorusso.parsimonia.data.local.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.stefanofattorusso.parsimonia.data.local.entity.TransactionEntity

@Dao
interface TransactionDao {

    @Query("SELECT * FROM `transaction`")
    suspend fun getAll(): List<TransactionEntity>

    @Query("SELECT * FROM `transaction` WHERE monthId = :monthId AND type != 2 AND fixed == 0 ORDER by timestamp DESC")
    fun getAllByMonth(monthId: String): PagingSource<Int, TransactionEntity>

    @Query("SELECT * FROM `transaction` WHERE monthId = :monthId AND fixed == 0 ORDER by timestamp DESC LIMIT :limit OFFSET :offset")
    fun getPagedList(monthId: String, limit: Int, offset: Int): List<TransactionEntity>

    @Query("SELECT * FROM `transaction` WHERE monthId = :monthId AND type != 0")
    suspend fun getExpensesByMonth(monthId: String): List<TransactionEntity>

    @Insert
    suspend fun insertAll(vararg transactionEntities: TransactionEntity)

    @Delete
    suspend fun delete(transactionEntity: TransactionEntity)
}
