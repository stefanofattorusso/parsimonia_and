package com.stefanofattorusso.parsimonia.data.local.mapper

import com.stefanofattorusso.common.extensions.toBoolean
import com.stefanofattorusso.common.extensions.toInt
import com.stefanofattorusso.parsimonia.data.local.entity.TransactionEntity
import com.stefanofattorusso.parsimonia.domain.model.TYPE
import com.stefanofattorusso.parsimonia.domain.model.Transaction
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject

class TransactionLocalMapper @Inject constructor() :
    com.stefanofattorusso.parsimonia.utils.Mapper<TransactionEntity, Transaction> {

    override fun mapFrom(unmapped: TransactionEntity): Transaction {
        return Transaction(
            id = unmapped.id,
            monthId = unmapped.monthId,
            name = unmapped.name,
            type = TYPE.fromInt(unmapped.type),
            amount = BigDecimal(unmapped.amount),
            timestamp = unmapped.timestamp,
            category = null,
            currency = null,
            fixed = unmapped.fixed.toBoolean()
        )
    }

    override fun mapTo(unmapped: Transaction): TransactionEntity {
        return TransactionEntity(
            id = unmapped.id ?: 0,
            monthId = unmapped.monthId,
            name = unmapped.name,
            type = unmapped.type.id,
            amount = unmapped.amount.toPlainString(),
            timestamp = unmapped.timestamp,
            catType = unmapped.category?.type?.toString()?.lowercase(Locale.getDefault()),
            catId = unmapped.category?.id,
            fixed = unmapped.fixed.toInt()
        )
    }
}
