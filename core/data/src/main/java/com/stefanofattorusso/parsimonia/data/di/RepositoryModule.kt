package com.stefanofattorusso.parsimonia.data.di

import com.stefanofattorusso.parsimonia.data.repository.CategoryRepositoryImpl
import com.stefanofattorusso.parsimonia.data.repository.CurrencyRepositoryImpl
import com.stefanofattorusso.parsimonia.data.repository.MonthRepositoryImpl
import com.stefanofattorusso.parsimonia.data.repository.TransactionRepositoryImpl
import com.stefanofattorusso.parsimonia.domain.repository.CategoryRepository
import com.stefanofattorusso.parsimonia.domain.repository.CurrencyRepository
import com.stefanofattorusso.parsimonia.domain.repository.MonthRepository
import com.stefanofattorusso.parsimonia.domain.repository.TransactionRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun provideMonthRepository(repository: MonthRepositoryImpl): MonthRepository

    @Binds
    abstract fun provideTransactionRepository(repository: TransactionRepositoryImpl): TransactionRepository

    @Binds
    abstract fun provideCategoryRepository(repository: CategoryRepositoryImpl): CategoryRepository

    @Binds
    abstract fun provideCurrencyRepository(repository: CurrencyRepositoryImpl): CurrencyRepository
}
