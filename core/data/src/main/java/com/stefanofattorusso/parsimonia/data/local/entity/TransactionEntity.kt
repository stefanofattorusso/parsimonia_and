package com.stefanofattorusso.parsimonia.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "transaction")
data class TransactionEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo val monthId: String,
    @ColumnInfo val name: String,
    @ColumnInfo val type: Int,      //income or outcome
    @ColumnInfo val amount: String,
    @ColumnInfo val timestamp: Long,
    @ColumnInfo val catType: String? = null,
    @ColumnInfo val catId: Int? = null,
    @ColumnInfo val fixed: Int = 0
)
