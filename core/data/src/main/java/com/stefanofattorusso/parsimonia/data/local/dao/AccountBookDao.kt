package com.stefanofattorusso.parsimonia.data.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.stefanofattorusso.parsimonia.data.local.entity.AccountBookEntity

@Dao
interface AccountBookDao {

    @Query("SELECT * FROM account_book")
    suspend fun getAll(): List<AccountBookEntity>

    @Insert
    suspend fun insertAll(vararg accountBookEntities: AccountBookEntity)

    @Delete
    suspend fun delete(accountBookEntity: AccountBookEntity)
}
