package com.stefanofattorusso.parsimonia.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "transactionName")
data class TransactionNameEntity(
    @PrimaryKey val name: String,
    @ColumnInfo val counter: Int = 0,
    @ColumnInfo val type: Int
)
