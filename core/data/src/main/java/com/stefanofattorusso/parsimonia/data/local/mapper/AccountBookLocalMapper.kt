package com.stefanofattorusso.parsimonia.data.local.mapper

import com.stefanofattorusso.parsimonia.data.local.entity.AccountBookEntity
import com.stefanofattorusso.parsimonia.domain.model.AccountBook
import javax.inject.Inject

class AccountBookLocalMapper @Inject constructor() :
    com.stefanofattorusso.parsimonia.utils.Mapper<AccountBookEntity, AccountBook> {

    override fun mapFrom(unmapped: AccountBookEntity): com.stefanofattorusso.parsimonia.domain.model.AccountBook {
        return com.stefanofattorusso.parsimonia.domain.model.AccountBook(unmapped.id)
    }

    override fun mapTo(unmapped: com.stefanofattorusso.parsimonia.domain.model.AccountBook): AccountBookEntity {
        return AccountBookEntity()
    }
}