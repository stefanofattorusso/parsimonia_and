package com.stefanofattorusso.parsimonia.data.repository

import com.stefanofattorusso.parsimonia.data.di.IoDispatcher
import com.stefanofattorusso.parsimonia.data.local.dao.MonthDao
import com.stefanofattorusso.parsimonia.data.local.dao.TransactionDao
import com.stefanofattorusso.parsimonia.data.local.dao.TransactionNameDao
import com.stefanofattorusso.parsimonia.data.local.entity.TransactionNameEntity
import com.stefanofattorusso.parsimonia.data.local.mapper.CategoryLocalMapper
import com.stefanofattorusso.parsimonia.data.local.mapper.MonthLocalMapper
import com.stefanofattorusso.parsimonia.data.local.mapper.TransactionLocalMapper
import com.stefanofattorusso.parsimonia.data.local.source.DeviceDataSource
import com.stefanofattorusso.parsimonia.domain.model.*
import com.stefanofattorusso.parsimonia.domain.repository.TransactionRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class TransactionRepositoryImpl @Inject constructor(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    private val transactionDao: TransactionDao,
    private val transactionLocalMapper: TransactionLocalMapper,
    private val monthDao: MonthDao,
    private val monthLocalMapper: MonthLocalMapper,
    private val transactionNameDao: TransactionNameDao,
    private val deviceDataSource: DeviceDataSource,
    private val categoryLocalMapper: CategoryLocalMapper,
) : TransactionRepository {

    override suspend fun getAllByMonth(monthId: String, limit: Int, offset: Int): List<Transaction> {
        return withContext(context = ioDispatcher) {
            val categoryList = deviceDataSource.getCategoryList().map { categoryLocalMapper.mapFrom(it) }
            val currency = deviceDataSource.retrieveDefaultCurrency()
            transactionDao.getPagedList(monthId, limit, offset).map { entity ->
                val result = transactionLocalMapper.mapFrom(entity)
                val category = categoryList.firstOrNull { it.id == entity.catId }
                result.copy(category = category, currency = currency)
            }
        }
    }

    override suspend fun getExpensesByMonth(monthId: String): List<Transaction> {
        return withContext(context = ioDispatcher) {
            transactionDao.getExpensesByMonth(monthId)
                .map { entity ->
                    val result = transactionLocalMapper.mapFrom(entity)
                    val categoryList = deviceDataSource.getCategoryList().map { categoryLocalMapper.mapFrom(it) }
                    result.copy(category = categoryList.firstOrNull { it.id == entity.catId })
                }
        }
    }

    override suspend fun create(transaction: Transaction) {
        withContext(context = ioDispatcher) {
            transactionDao.insertAll(transactionLocalMapper.mapTo(transaction))
            updateMonth(transaction)
            if (transaction.type != TYPE.SERVICE && transaction.fixed != true) {
                addName(transaction.name, transaction.type.id)
            }
        }
    }

    private suspend fun updateMonth(transaction: Transaction) {
        val monthEntity = monthDao.get(transaction.monthId).firstOrNull()
        if (monthEntity != null) {
            var month = monthLocalMapper.mapFrom(monthEntity)
            month = when (transaction.type) {
                TYPE.INCOME -> month.updateWithIncome(transaction.amount)
                TYPE.OUTCOME -> month.updateWithOutcome(transaction.amount, transaction.fixed)
                TYPE.SERVICE -> month.updateWithService(transaction.amount)
                else -> month
            }
            monthDao.update(monthLocalMapper.mapTo(month))
        }
    }

    override suspend fun getSuggestedNameList(type: TYPE): List<String> {
        return withContext(context = ioDispatcher) {
            transactionNameDao.getMostUsedNames(type.id)
        }
    }

    private suspend fun addName(name: String, type: Int) {
        withContext(context = ioDispatcher) {
            transactionNameDao.insert(TransactionNameEntity(name = name, type = type))
            transactionNameDao.updateCounter(name)
        }
    }
}
