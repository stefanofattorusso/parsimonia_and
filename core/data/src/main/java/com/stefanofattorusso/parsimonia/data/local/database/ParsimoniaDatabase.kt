package com.stefanofattorusso.parsimonia.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.stefanofattorusso.parsimonia.data.local.dao.AccountBookDao
import com.stefanofattorusso.parsimonia.data.local.dao.MonthDao
import com.stefanofattorusso.parsimonia.data.local.dao.TransactionDao
import com.stefanofattorusso.parsimonia.data.local.dao.TransactionNameDao
import com.stefanofattorusso.parsimonia.data.local.entity.AccountBookEntity
import com.stefanofattorusso.parsimonia.data.local.entity.MonthEntity
import com.stefanofattorusso.parsimonia.data.local.entity.TransactionEntity
import com.stefanofattorusso.parsimonia.data.local.entity.TransactionNameEntity

@Database(
    entities = [AccountBookEntity::class, MonthEntity::class, TransactionEntity::class, TransactionNameEntity::class],
    version = 1
)
abstract class ParsimoniaDatabase : RoomDatabase() {
    abstract fun accountBookDao(): AccountBookDao
    abstract fun monthDao(): MonthDao
    abstract fun transactionDao(): TransactionDao
    abstract fun transactionNameDao(): TransactionNameDao
}