package com.stefanofattorusso.parsimonia.data.local.mapper

import android.content.Context
import com.stefanofattorusso.parsimonia.data.local.entity.CategoryEntity
import com.stefanofattorusso.parsimonia.domain.model.Category
import dagger.hilt.android.qualifiers.ApplicationContext
import java.util.*
import javax.inject.Inject

class CategoryLocalMapper @Inject constructor(
    @ApplicationContext private val context: Context
) : com.stefanofattorusso.parsimonia.utils.Mapper<CategoryEntity, Category> {

    override fun mapFrom(unmapped: CategoryEntity): com.stefanofattorusso.parsimonia.domain.model.Category {
        return com.stefanofattorusso.parsimonia.domain.model.Category(
            unmapped.id,
            unmapped.name,
            com.stefanofattorusso.parsimonia.domain.model.CategoryType.valueOf(
                unmapped.type.uppercase(
                    Locale.getDefault()
                )
            ),
            getResId(unmapped.icon)
        )
    }

    override fun mapTo(unmapped: com.stefanofattorusso.parsimonia.domain.model.Category): CategoryEntity {
        TODO("Not yet implemented")
    }

    private fun getResId(name: String): Int {
        return context.resources.getIdentifier(name, "drawable", context.packageName)
    }
}
