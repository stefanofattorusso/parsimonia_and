package com.stefanofattorusso.parsimonia.feature.transaction.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import com.google.android.material.chip.Chip
import com.stefanofattorusso.parsimonia.domain.model.TYPE
import com.stefanofattorusso.parsimonia.feature.transaction.databinding.TransactionAddOutcomeViewBinding

class TransactionAddOutcomeView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : FrameLayout(context, attrs, defStyleAttr) {

    private val binding = TransactionAddOutcomeViewBinding.inflate(LayoutInflater.from(context), this, true)
    var onAddClicked: (TransactionAddModel) -> Unit = {}

    init {
        binding.setUp()
    }

    @Suppress("UNCHECKED_CAST")
    private fun TransactionAddOutcomeViewBinding.setUp() {
        addButton.setOnClickListener { onAddClicked(buildModel()) }
        nameInput.doAfterTextChanged { toggleAddButton() }
        amountInput.doAfterTextChanged { toggleAddButton() }
        chipBasic.setOnClickListener { view -> buildCategoriesChips(view.tag as List<com.stefanofattorusso.parsimonia.domain.model.Category>) }
        chipOptional.setOnClickListener { view -> buildCategoriesChips(view.tag as List<com.stefanofattorusso.parsimonia.domain.model.Category>) }
        chipFreeTime.setOnClickListener { view -> buildCategoriesChips(view.tag as List<com.stefanofattorusso.parsimonia.domain.model.Category>) }
        chipExtras.setOnClickListener { view -> buildCategoriesChips(view.tag as List<com.stefanofattorusso.parsimonia.domain.model.Category>) }
        fixedExpenseCheckbox.setOnCheckedChangeListener { _, isChecked ->
            showOrClearAndHideCategories(!isChecked)
            toggleAddButton()
        }
    }

    private fun showOrClearAndHideCategories(show: Boolean) {
        if (!show) {
            binding.categoryGroup.clearCheck()
            binding.categoryGroup.removeAllViews()
            binding.categoryTypeGroup.clearCheck()
        }
        binding.categoryContainer.isVisible = show
    }

    fun setModel(model: TransactionAddModel) {
        binding.nameInput.setText(model.name)
        binding.amountInput.setText(model.amount)
    }

    fun setSuggestions(list: List<String>) {
        buildTypeChips(list)
    }

    private fun buildTypeChips(names: List<String>) {
        binding.chipGroup.removeAllViews()
        binding.chipsContainer.isVisible = names.isNotEmpty()
        names.forEach {
            binding.chipGroup.addView(
                Chip(context).apply {
                    text = it
                    setOnClickListener {
                        binding.nameInput.setText(text)
                    }
                }
            )
        }
    }

    fun setCategories(list: List<com.stefanofattorusso.parsimonia.domain.model.Category>) {
        binding.chipBasic.tag = list.filter { it.type == com.stefanofattorusso.parsimonia.domain.model.CategoryType.BASIC_NECESSITIES }
        binding.chipOptional.tag = list.filter { it.type == com.stefanofattorusso.parsimonia.domain.model.CategoryType.OPTIONAL }
        binding.chipFreeTime.tag = list.filter { it.type == com.stefanofattorusso.parsimonia.domain.model.CategoryType.CULTURE_FREE_TIME }
        binding.chipExtras.tag = list.filter { it.type == com.stefanofattorusso.parsimonia.domain.model.CategoryType.EXTRAS }
    }

    private fun buildModel() = TransactionAddModel(
        type = TYPE.OUTCOME,
        name = binding.nameInput.text.toString(),
        amount = binding.amountInput.text.toString(),
        category = findCategoryByView(),
        isFixed = binding.fixedExpenseCheckbox.isChecked
    )

    private fun findCategoryByView(): com.stefanofattorusso.parsimonia.domain.model.Category? {
        val viewId = binding.categoryGroup.checkedChipId
        if (viewId == View.NO_ID) return null
        val chipView = binding.categoryGroup.findViewById<CategoryChip>(viewId)
        return chipView.category
    }

    private fun toggleAddButton() {
        binding.addButton.isEnabled = areNameAndAmountFilled() && (isFixedChecked() || isCategorySelected())
    }

    private fun areNameAndAmountFilled() =
        !binding.nameInput.text.isNullOrBlank() && !binding.amountInput.text.isNullOrBlank()

    private fun isFixedChecked() = binding.fixedExpenseCheckbox.isChecked

    private fun isCategorySelected() = findCategoryByView() != null

    private fun buildCategoriesChips(categories: List<com.stefanofattorusso.parsimonia.domain.model.Category>) {
        binding.categoryGroup.removeAllViews()
        binding.categoryContainer.isVisible = categories.isNotEmpty()
        categories.forEach {
            binding.categoryGroup.addView(CategoryChip.buildChip(context, it).apply {
                setOnActionListener { toggleAddButton() }
            })
        }
    }
}
