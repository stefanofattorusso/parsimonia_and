package com.stefanofattorusso.parsimonia.feature.transaction.view

import android.content.Context
import android.util.TypedValue
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StyleRes
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.children
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.stefanofattorusso.parsimonia.domain.model.Category
import com.stefanofattorusso.parsimonia.feature.transaction.R

class CategoryChip(
    context: Context,
    val category: Category,
    @StyleRes style: Int,
    @ColorRes color: Int,
    smallSize: Boolean?
) : Chip(ContextThemeWrapper(context, style)) {

    init {
        text = category.name
        isCheckable = true
        chipStartPadding = resources.getDimensionPixelSize(R.dimen.small_margin).toFloat()
        if (smallSize == true) {
            setTextSize(TypedValue.COMPLEX_UNIT_SP, 13f)
        }
        setUpIcon(category.icon, color, smallSize)
    }

    fun setOnActionListener(action: (Int) -> Unit) {
        setOnClickListener {
            isChipIconVisible = !isChecked
            action.invoke(category.id)
            val group = parent
            if (group is ChipGroup) {
                group.children.iterator().forEach { child ->
                    (child as Chip).isChipIconVisible = this != child
                }
            }
        }
    }

    private fun setUpIcon(@DrawableRes icon: Int?, @ColorRes color: Int, smallSize: Boolean?) {
        icon?.takeIf { it > 0 }?.let {
            chipIcon = ResourcesCompat.getDrawable(resources, icon, null)
            chipIconTint = ResourcesCompat.getColorStateList(resources, color, null)
            if (smallSize == true) {
                chipIconSize = resources.getDimensionPixelSize(R.dimen.cat_small_icon_size).toFloat()
            }
        }
    }

    companion object {
        fun buildChip(context: Context, category: com.stefanofattorusso.parsimonia.domain.model.Category, smallSize: Boolean? = false): CategoryChip {
            val style: Int
            val color: Int
            when (category.type) {
                com.stefanofattorusso.parsimonia.domain.model.CategoryType.BASIC_NECESSITIES -> {
                    style = R.style.BaseChip_BasicCatChip
                    color = R.color.cat_basic
                }
                com.stefanofattorusso.parsimonia.domain.model.CategoryType.OPTIONAL -> {
                    style = R.style.BaseChip_OptionalCatChip
                    color = R.color.cat_optional
                }
                com.stefanofattorusso.parsimonia.domain.model.CategoryType.CULTURE_FREE_TIME -> {
                    style = R.style.BaseChip_CultureCatChip
                    color = R.color.cat_culture
                }
                com.stefanofattorusso.parsimonia.domain.model.CategoryType.EXTRAS -> {
                    style = R.style.BaseChip_ExtrasCatChip
                    color = R.color.cat_extras
                }
            }
            return CategoryChip(context, category, style, color, smallSize)
        }
    }
}
