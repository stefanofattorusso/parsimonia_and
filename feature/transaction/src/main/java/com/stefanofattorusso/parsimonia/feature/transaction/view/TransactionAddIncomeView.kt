package com.stefanofattorusso.parsimonia.feature.transaction.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import com.google.android.material.chip.Chip
import com.stefanofattorusso.parsimonia.domain.model.TYPE
import com.stefanofattorusso.parsimonia.feature.transaction.databinding.TransactionAddIncomeViewBinding

class TransactionAddIncomeView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : FrameLayout(context, attrs, defStyleAttr) {

    private val binding = TransactionAddIncomeViewBinding.inflate(LayoutInflater.from(context), this, true)
    var onAddClicked: (TransactionAddModel) -> Unit = {}

    init {
        binding.addButton.setOnClickListener { onAddClicked(buildModel()) }
        binding.nameInput.doAfterTextChanged { toggleAddButton() }
        binding.amountInput.doAfterTextChanged { toggleAddButton() }
    }

    fun setModel(model: TransactionAddModel) {
        binding.nameInput.setText(model.name)
        binding.amountInput.setText(model.amount)
    }

    fun setSuggestions(list: List<String>) {
        buildTypeChips(list)
    }

    private fun buildTypeChips(names: List<String>) {
        binding.chipGroup.removeAllViews()
        binding.chipsContainer.isVisible = names.isNotEmpty()
        names.forEach {
            binding.chipGroup.addView(
                Chip(context).apply {
                    text = it
                    setOnClickListener {
                        binding.nameInput.setText(text)
                    }
                }
            )
        }
    }

    private fun buildModel() = TransactionAddModel(
        type = TYPE.INCOME,
        name = binding.nameInput.text.toString(),
        amount = binding.amountInput.text.toString(),
    )

    private fun toggleAddButton() {
        binding.addButton.isEnabled =
            !binding.nameInput.text.isNullOrBlank() && !binding.amountInput.text.isNullOrBlank()
    }
}
