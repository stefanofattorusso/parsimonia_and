package com.stefanofattorusso.parsimonia.feature.transaction.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.core.view.children
import androidx.core.widget.doAfterTextChanged
import com.google.android.material.chip.Chip
import com.stefanofattorusso.parsimonia.domain.model.TYPE
import com.stefanofattorusso.parsimonia.feature.transaction.databinding.TransactionAddServiceViewBinding

class TransactionAddServiceView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : FrameLayout(context, attrs, defStyleAttr) {

    private val binding = TransactionAddServiceViewBinding.inflate(LayoutInflater.from(context), this, true)
    var onAddClicked: (TransactionAddModel) -> Unit = {}

    init {
        binding.addButton.setOnClickListener { onAddClicked(buildModel()) }
        binding.amountInput.doAfterTextChanged { toggleAddButton() }
        binding.serviceTypeGroup.setOnCheckedStateChangeListener { _, _ -> toggleAddButton() }
    }

    fun setModel(model: TransactionAddModel) {
        binding.amountInput.setText(model.amount)
    }

    private fun buildModel() = TransactionAddModel(
        type = TYPE.SERVICE,
        name = findSelectedName(),
        amount = binding.amountInput.text.toString(),
    )

    private fun findSelectedName(): String {
        val chipId = binding.serviceTypeGroup.checkedChipId
        val chipView = binding.serviceTypeGroup.children.find { it.id == chipId } as Chip
        return chipView.text.toString()
    }

    private fun toggleAddButton() {
        binding.addButton.isEnabled = isANameSelected() && !binding.amountInput.text.isNullOrBlank()
    }

    private fun isANameSelected() = binding.serviceTypeGroup.checkedChipId != View.NO_ID
}
