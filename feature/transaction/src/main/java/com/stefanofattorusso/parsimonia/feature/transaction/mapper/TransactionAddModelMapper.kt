package com.stefanofattorusso.parsimonia.feature.transaction.mapper

import com.stefanofattorusso.common.extensions.format
import com.stefanofattorusso.parsimonia.domain.model.Transaction
import com.stefanofattorusso.parsimonia.feature.transaction.view.TransactionAddModel
import com.stefanofattorusso.parsimonia.utils.Mapper
import java.math.BigDecimal
import javax.inject.Inject

class TransactionAddModelMapper @Inject constructor() : Mapper<Transaction, TransactionAddModel> {

    override fun mapFrom(unmapped: Transaction): TransactionAddModel {
        return TransactionAddModel(
            name = unmapped.name,
            type = unmapped.type,
            amount = unmapped.amount.format(unmapped.currency),
            category = unmapped.category,
            isFixed = unmapped.fixed,
        )
    }

    override fun mapTo(unmapped: TransactionAddModel): Transaction {
        return Transaction(
            name = unmapped.name,
            amount = BigDecimal(unmapped.amount),
            category = unmapped.category,
            fixed = unmapped.isFixed,
            timestamp = System.currentTimeMillis(),
            monthId = "",
            type = unmapped.type,
        )
    }
}
