package com.stefanofattorusso.parsimonia.feature.transaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.stefanofattorusso.parsimonia.domain.model.TYPE
import com.stefanofattorusso.parsimonia.domain.usecase.AddTransactionUseCase
import com.stefanofattorusso.parsimonia.domain.usecase.GetExpenseCategoriesUseCase
import com.stefanofattorusso.parsimonia.domain.usecase.GetNameSuggestionsUseCase
import com.stefanofattorusso.parsimonia.feature.transaction.mapper.TransactionAddModelMapper
import com.stefanofattorusso.parsimonia.feature.transaction.view.TransactionAddModel
import com.stefanofattorusso.parsimonia.utils.ResultData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TransactionAddViewModel @Inject constructor(
    private val addTransactionUseCase: AddTransactionUseCase,
    private val getExpenseCategoriesUseCase: GetExpenseCategoriesUseCase,
    private val transactionAddModelMapper: TransactionAddModelMapper,
    private val getNameSuggestionsUseCase: GetNameSuggestionsUseCase,
) : ViewModel() {

    private val _suggestedNames = MutableLiveData<List<String>>()
    val suggestedNames: LiveData<List<String>> = _suggestedNames

    private val _categoryList = MutableLiveData<List<com.stefanofattorusso.parsimonia.domain.model.Category>>()
    val categoryList: LiveData<List<com.stefanofattorusso.parsimonia.domain.model.Category>> = _categoryList

    private val _viewState = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState> = _viewState

    private lateinit var monthId: String

    fun setMonthId(id: String) {
        monthId = id
        retrieveCategories()
    }

    fun setType(type: TYPE) {
        retrieveSuggestedNames(type)
        when (type) {
            TYPE.INCOME -> _viewState.value = ViewState.Income
            TYPE.OUTCOME -> _viewState.value = ViewState.Outcome
            TYPE.SERVICE -> _viewState.value = ViewState.Service
            else -> Unit
        }
    }

    private fun retrieveSuggestedNames(type: TYPE) {
        viewModelScope.launch {
            when (val result = getNameSuggestionsUseCase(GetNameSuggestionsUseCase.Params(type))) {
                is ResultData.Success -> _suggestedNames.value = result.value
                else -> Unit
            }
        }
    }

    private fun retrieveCategories() {
        viewModelScope.launch {
            when (val result = getExpenseCategoriesUseCase(GetExpenseCategoriesUseCase.Params())) {
                is ResultData.Success -> _categoryList.postValue(result.value)
                else -> Unit
            }
        }
    }

    fun addTransaction(input: TransactionAddModel) {
        viewModelScope.launch {
            val newTransaction = transactionAddModelMapper.mapTo(input).copy(monthId = monthId)
            when (addTransactionUseCase(AddTransactionUseCase.Params(newTransaction))) {
                is ResultData.Success -> _viewState.value = ViewState.Success
                is ResultData.Error -> _viewState.value = ViewState.Failure
            }
        }
    }

    sealed class ViewState {
        object Income : ViewState()
        object Outcome : ViewState()
        object Service : ViewState()
        object Success : ViewState()
        object Failure : ViewState()
    }
}
