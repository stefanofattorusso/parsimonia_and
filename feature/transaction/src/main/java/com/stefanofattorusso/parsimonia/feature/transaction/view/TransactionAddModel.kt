package com.stefanofattorusso.parsimonia.feature.transaction.view

import com.stefanofattorusso.parsimonia.domain.model.TYPE

data class TransactionAddModel(
    val type: TYPE,
    val name: String,
    val amount: String,
    val isFixed: Boolean? = null,
    val category: com.stefanofattorusso.parsimonia.domain.model.Category? = null
) {

    companion object {

        fun empty() = TransactionAddModel(
            type = TYPE.UNDEFINED,
            name = "",
            amount = "",
            isFixed = null,
            category = null,
        )
    }
}
