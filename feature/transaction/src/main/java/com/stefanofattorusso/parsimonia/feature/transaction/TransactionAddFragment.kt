package com.stefanofattorusso.parsimonia.feature.transaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.stefanofattorusso.parsimonia.feature.transaction.R
import com.stefanofattorusso.parsimonia.domain.model.TYPE
import com.stefanofattorusso.parsimonia.feature.transaction.databinding.FragmentTransactionAddBinding
import com.stefanofattorusso.parsimonia.ui.base.BaseFragment
import com.stefanofattorusso.parsimonia.ui.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TransactionAddFragment : BaseFragment<FragmentTransactionAddBinding>() {

    private val viewModel: TransactionAddViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val monthId = arguments?.getString("MONTH_ID")
        if (monthId != null) {
            viewModel.setMonthId(monthId)
        } else {
            requireActivity().finish()
        }
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): FragmentTransactionAddBinding {
        return FragmentTransactionAddBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.setUp()
        observeData()
    }

    private fun FragmentTransactionAddBinding.setUp() {
        closeButton.setOnClickListener { requireActivity().finish() }
        typeRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.income_radio_button -> viewModel.setType(TYPE.INCOME)
                R.id.outcome_radio_button -> viewModel.setType(TYPE.OUTCOME)
                R.id.service_radio_button -> viewModel.setType(TYPE.SERVICE)
            }
        }
        incomeView.onAddClicked = { input -> viewModel.addTransaction(input) }
        outcomeView.onAddClicked = { input -> viewModel.addTransaction(input) }
        serviceView.onAddClicked = { input -> viewModel.addTransaction(input) }
    }

    private fun observeData() {
        observe(viewModel.viewState) { viewState ->
            when (viewState) {
                is TransactionAddViewModel.ViewState.Income -> binding.showIncomeUI()
                is TransactionAddViewModel.ViewState.Outcome -> binding.showExpenseUI()
                is TransactionAddViewModel.ViewState.Service -> binding.showServiceUI()
                is TransactionAddViewModel.ViewState.Success -> activity?.finish()
                else -> Unit
            }
        }
        observe(viewModel.categoryList) { list ->
            binding.outcomeView.setCategories(list)
        }
        observe(viewModel.suggestedNames) { list ->
            if (binding.incomeView.isVisible) {
                binding.incomeView.setSuggestions(list)
            } else if (binding.outcomeView.isVisible) {
                binding.outcomeView.setSuggestions(list)
            }
        }
    }

    private fun FragmentTransactionAddBinding.showIncomeUI() {
        incomeView.isVisible = true
        outcomeView.isVisible = false
        serviceView.isVisible = false
    }

    private fun FragmentTransactionAddBinding.showExpenseUI() {
        incomeView.isVisible = false
        outcomeView.isVisible = true
        serviceView.isVisible = false
    }

    private fun FragmentTransactionAddBinding.showServiceUI() {
        incomeView.isVisible = false
        outcomeView.isVisible = false
        serviceView.isVisible = true
    }
}
