package com.stefanofattorusso.parsimonia.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.stefanofattorusso.parsimonia.domain.usecase.GetCurrencyListUseCase
import com.stefanofattorusso.parsimonia.domain.usecase.GetCurrentCurrencyUseCase
import com.stefanofattorusso.parsimonia.domain.usecase.SaveCurrentCurrencyUseCase
import com.stefanofattorusso.parsimonia.utils.ResultData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val saveCurrentCurrencyUseCase: SaveCurrentCurrencyUseCase,
    private val getCurrentCurrencyUseCase: GetCurrentCurrencyUseCase,
    private val getCurrencyListUseCase: GetCurrencyListUseCase,
) : ViewModel() {

    private val _currencies = MutableLiveData<List<Currency>>()
    val currencies: LiveData<List<Currency>> = _currencies

    private val _currency = MutableLiveData<Currency>()
    val currency: LiveData<Currency> = _currency

    init {
        retrieveDefaultCurrency()
        retrieveAvailableCurrencies()
    }

    fun setNewCurrency(currency: String) {
        viewModelScope.launch {
            when (saveCurrentCurrencyUseCase(SaveCurrentCurrencyUseCase.Params(currency))) {
                is ResultData.Success -> retrieveDefaultCurrency()
                else -> Unit
            }
        }
    }

    private fun retrieveDefaultCurrency() {
        viewModelScope.launch {
            when (val result = getCurrentCurrencyUseCase(GetCurrentCurrencyUseCase.Params())) {
                is ResultData.Success -> _currency.value = Currency.getInstance(result.value)
                else -> Unit
            }
        }
    }

    private fun retrieveAvailableCurrencies() {
        viewModelScope.launch {
            when (val result = getCurrencyListUseCase(GetCurrencyListUseCase.Params())) {
                is ResultData.Success -> _currencies.value = result.value
                else -> Unit
            }
        }
    }
}