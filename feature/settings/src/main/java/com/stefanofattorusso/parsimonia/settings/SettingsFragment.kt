package com.stefanofattorusso.parsimonia.settings

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.preference.DropDownPreference
import androidx.preference.PreferenceFragmentCompat
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SettingsFragment : PreferenceFragmentCompat() {

    private val viewModel: SettingsViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObservers()

        preferenceScreen.findPreference<DropDownPreference>(CURRENCY_KEY)?.run {
            setOnPreferenceChangeListener { _, newValue ->
                viewModel.setNewCurrency(newValue as String)
                activity?.setResult(Activity.RESULT_OK)
                true
            }
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.fragment_settings, rootKey)
    }

    private fun initObservers() {
        viewModel.currencies.observe(viewLifecycleOwner) { list ->
            val currencies = list.map { it.symbol + " - " + it.displayName }
            val values = list.map { it.currencyCode }

            preferenceScreen.findPreference<DropDownPreference>(CURRENCY_KEY)?.run {
                entries = currencies.toTypedArray()
                entryValues = values.toTypedArray()
            }
        }

        viewModel.currency.observe(viewLifecycleOwner) { defaultCurrency ->
            preferenceScreen.findPreference<DropDownPreference>(CURRENCY_KEY)?.run {
                summary = getString(R.string.default_currency, defaultCurrency.symbol, defaultCurrency.currencyCode)
            }
        }
    }

    companion object {
        private const val CURRENCY_KEY = "currency"
    }
}
