package com.stefanofattorusso.parsimonia.settings

import android.os.Bundle
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.stefanofattorusso.parsimonia.settings.databinding.ActivitySettingsBinding
import com.stefanofattorusso.parsimonia.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SettingsActivity : BaseActivity<ActivitySettingsBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.title = getString(R.string.settings_title)
        }

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                add<SettingsFragment>(
                    R.id.container,
                    null,
                    intent.extras
                )
            }
        }
    }

    override fun getViewBinding() = ActivitySettingsBinding.inflate(layoutInflater)
}
