# Parsimonia

A simple application to keep your daily expenses under control and help you save money.
This application is inspired by Kakebo (from the Japanese "house account book").

**This is a Working in Progress project.**

The goal of this project is to do practice on the new technologies, patterns and styles.
Some functionalities are still not implemented or not completed.

* Language: **Kotlin**
* App Architecture: **Clean Architecture, MVVM**
* Database: **Room**
* Concurrency design pattern: **Coroutines**
* Dependency Injection: **Dagger Hilt**
* UI: **Jetpack Compose [TBI]**
* Code style: **Clean Code**
* Testing: **Unit tests**
* Modularization: **Work in Progress**